<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\CoordinadorController;
use App\Http\Controllers\Admin\ClienteController;
use App\Http\Controllers\Admin\RelacionController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\GestionCoordinadorController;
use App\Http\Controllers\Admin\GestionClienteController;
use App\Http\Controllers\Admin\SettingsController;

use App\Http\Controllers\Coordinador\FilesController;

use App\Http\Controllers\Cliente\FilesController as FilesClientController;
use App\Http\Controllers\Cliente\AjustesController;

use App\Http\Controllers\Auth\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
Route::post('login_post', [LoginController::class,'postLogin']);
Route::post('logout', [LoginController::class,'logout']);

Route::get('/', [HomeController::class,'index']);

Route::group(['prefix' => 'admin','middleware' => 'auth'], function(){
    Route::group(['prefix' => 'coordinador'], function(){
        Route::get('/',[CoordinadorController::class,'index'])->name('admin.coordinador.index');
        Route::get('get',[CoordinadorController::class,'get'])->name('admin.coordinador.get');
        Route::post('store',[CoordinadorController::class,'store'])->name('admin.coordinador.store');
        Route::group(['prefix' => '{id}'], function(){
            Route::get('edit',[CoordinadorController::class,'edit']);
            Route::put('update',[CoordinadorController::class,'update']);
            Route::put('update_pass',[CoordinadorController::class,'update_pass']);

            Route::group(['prefix' => 'relaciones'], function(){
                Route::get('/',[RelacionController::class,'index']);
                Route::get('get_clients',[RelacionController::class,'get_clients']);
                Route::get('get',[RelacionController::class,'get_relations']);//Relaciones del coordinador
                Route::post('store',[RelacionController::class,'store']);
                Route::get('{id_rel}/destroy',[RelacionController::class,'destroy']);
            });
        });

        //Enviados coordinador
        Route::get('enviados',[GestionCoordinadorController::class,'enviados'])->name('admin.coord.enviados'); 
        Route::get('get_enviados',[GestionCoordinadorController::class,'get_enviados'])->name('admin.coord.get_enviados');
        Route::get('{id}/rec_eliminado_coord',[GestionCoordinadorController::class,'rec_eliminado_coord']);
        Route::get('{id}/rec_eliminado_cliente',[GestionCoordinadorController::class,'rec_eliminado_cliente']);
        Route::post('eliminar_enviados',[GestionCoordinadorController::class,'eliminar_enviados'])->name('admin.coord.eliminar_enviados'); 
    });  

    Route::group(['prefix' => 'cliente'], function(){
        Route::get('/',[ClienteController::class,'index'])->name('admin.cliente.index');
        Route::get('get',[ClienteController::class,'get'])->name('admin.cliente.get');
        Route::post('store',[ClienteController::class,'store'])->name('admin.cliente.store');
        Route::get('{id}/edit',[ClienteController::class,'edit']);
        Route::put('{id}/update',[ClienteController::class,'update']);
        Route::put('{id}/update_pass',[ClienteController::class,'update_pass']);

        //Enviados cliente
        Route::get('enviados',[GestionClienteController::class,'enviados'])->name('admin.cliente.enviados'); 
        Route::get('get_enviados',[GestionClienteController::class,'get_enviados'])->name('admin.cliente.get_enviados');
        Route::get('{id}/rec_eliminado_coord',[GestionClienteController::class,'rec_eliminado_coord']);
        Route::get('{id}/rec_eliminado_cliente',[GestionClienteController::class,'rec_eliminado_cliente']);
        Route::post('eliminar_enviados',[GestionClienteController::class,'eliminar_enviados'])->name('admin.cliente.eliminar_enviados');
    });

    Route::group(['prefix' => 'users'], function(){
        Route::get('/',[UsersController::class,'index'])->name('admin.users.index');
        Route::post('store',[UsersController::class,'store'])->name('admin.users.store');
        Route::get('get',[UsersController::class,'get_users'])->name('admin.users.get');
        Route::get('{id}/edit',[UsersController::class,'edit']);
        Route::put('{id}/update',[UsersController::class,'update']);
        Route::put('{id}/update_password',[UsersController::class,'update_password']);
    });

    Route::group(['prefix' => 'settings'], function(){
        Route::get('/',[SettingsController::class,'index'])->name('admin.settings');
        Route::get('reset_folder_downloads',[SettingsController::class,'reset_folder_downloads'])->name('admin.reset_folder_downloads');
    });
});

Route::group(['prefix' => 'coordinador','middleware'=>'auth'], function(){

    Route::get('select_cliente',[FilesController::class,'select_client'])->name('coord.select_client');
    Route::get('get_clients',[FilesController::class,'get_clients'])->name('coord.get_clients');
    Route::get('cliente_{id}/view_send',[FilesController::class,'view_send']);
    Route::post('cliente_{id}/send',[FilesController::class,'send']);

    //ENVIADOS DEL COORDINADOR
    Route::get('files_sended',[FilesController::class,'files_sended'])->name('coord.files_sended');
    Route::get('get_files_sended',[FilesController::class,'get_files_sended'])->name('coord.get_files_sended');
    Route::post('delete_files_sended',[FilesController::class,'delete_files_sended'])->name('coord.delete_files_sended');
    Route::post('download_files_sended',[FilesController::class,'download_files_sended'])->name('coord.download_files_sended');
    Route::get('download_file_{id}',[FilesController::class,'download_file']);

    //RECIBIDOS DEL COORDINADOR
    Route::get('received_files',[FilesController::class,'received_files'])->name('coord.received_files');
    Route::get('get_received_files',[FilesController::class,'get_received_files'])->name('coord.get_received_files');
    Route::post('delete_received_files',[FilesController::class,'delete_received_files'])->name('coord.delete_received_files');
    Route::post('download_received_files',[FilesController::class,'download_received_files'])->name('coord.download_received_files');
    Route::get('download_file_{id}/received',[FilesController::class,'download_file_received']);
});


Route::group(['prefix' => 'cliente','middleware'=>'auth'], function(){
    Route::get('select_coord',[FilesClientController::class,'select_coord'])->name('cliente.select_coord');
    Route::get('get_coords',[FilesClientController::class,'get_coords'])->name('cliente.get_coords');
    Route::get('coord_{id}/view_send',[FilesClientController::class,'view_send']);
    Route::post('coord_{id}/send',[FilesClientController::class,'send']);

    //ENVIADOS DEL CLIENTE
    Route::get('files_sended',[FilesClientController::class,'files_sended'])->name('cliente.files_sended');
    Route::get('get_files_sended',[FilesClientController::class,'get_files_sended'])->name('cliente.get_files_sended');
    Route::post('delete_files_sended',[FilesClientController::class,'delete_files_sended'])->name('cliente.delete_files_sended');
    Route::post('download_files_sended',[FilesClientController::class,'download_files_sended'])->name('cliente.download_files_sended');
    Route::get('download_file_{id}',[FilesClientController::class,'download_file']);

    //RECIBIDOS DEL CLIENTE
    Route::get('received_files',[FilesClientController::class,'received_files'])->name('cliente.received_files');
    Route::get('get_received_files',[FilesClientController::class,'get_received_files'])->name('cliente.get_received_files');
    Route::post('delete_received_files',[FilesClientController::class,'delete_received_files'])->name('cliente.delete_received_files');
    Route::post('download_received_files',[FilesClientController::class,'download_received_files'])->name('cliente.download_received_files');
    Route::get('download_file_{id}/received',[FilesClientController::class,'download_file_received']);

    //AJUSTES
    Route::get('change_password',[AjustesController::class,'index'])->name('cliente.ajustes');
    Route::put('change_password',[AjustesController::class,'update_password'])->name('cliente.update_password');
});


/*
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
*/

Route::view('401','errors.401');
Route::view('401_inactive','errors.401_inactive');
