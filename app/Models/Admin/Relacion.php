<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Cliente;
use App\Models\Admin\Coordinador;

class Relacion extends Model
{
    use HasFactory;

    protected $table = 'relacion';
    protected $primaryKey = 'id';
    protected $fillable = [

        'coordinador_id',
        'cliente_id'
    ];

    
    //Relationships
    public function cliente(){
        return $this->belongsTo(new Cliente, 'cliente_id');
    }

    public function coordinador(){
        return $this->belongsTo(new Coordinador, 'coordinador_id');
    }
}
