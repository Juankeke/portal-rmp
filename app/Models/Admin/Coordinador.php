<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\User;
use App\Models\Admin\Relacion;

class Coordinador extends Model
{
    use HasFactory;

    protected $table = 'coordinador';
    protected $primaryKey = 'id';
    protected $fillable = [

        'paterno',
        'materno',
        'nombre',
        'email'
    ];

    protected $appends = ['full_name'];

    //Relationships
    public function user(){
        return $this->hasOne(new User, 'coordinador_id');
    }

    public function relaciones(){
        return $this->hasMany(new Relacion, 'coordinador_id');   
    }

    //Accesors
    public function getFullNameAttribute(){
        return $this->nombre.' '.$this->paterno.' '.$this->materno;
    }
}
