<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Role;
use App\Models\Admin\Coordinador;
use App\Models\Admin\Cliente;

class User extends Authenticatable
{
    use HasFactory;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = [

        'role_id',
        'coordinador_id',
        'cliente_id',
        'paterno',
        'materno',
        'nombre',
        'username',
        'password',
        'remember_token',
        'status' //1 ACTIVO, 0 INACTIVO
    ];

    protected $appends = ['full_name','status_user'];

    //Relationships
    public function role(){
        return $this->belongsTo(new Role,'role_id');
    }

    public function coordinador(){
        return $this->belongsTo(new Coordinador,'coordinador_id');
    }

    public function cliente(){
        return $this->belongsTo(new Cliente,'cliente_id');
    }

    //Accesors
    public function getFullNameAttribute(){
        return $this->paterno.' '.$this->materno.' '.$this->nombre;
    }

    public function getStatusUserAttribute(){
        switch ($this->status) {
            case 1:
                return "<font color='#26CE3B'>ACTIVO</font>";
                break;
            case 0:
                return "<font color='#CE2626'>INACTIVO</font>";
                break;
            default:
                return "DEFAULT CASE STATUS USER";
                break;
        }
    }
}
