<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\User;
use App\Models\Admin\Relacion;

class Cliente extends Model
{
    use HasFactory;

    protected $table = 'cliente';
    protected $primaryKey = 'id';
    protected $fillable = [

        'razon_social',
        'rfc',
        'email'
    ];

    //Relationships
    public function user(){
        return $this->hasOne(new User, 'cliente_id');
    }

    public function relaciones(){
        return $this->hasMany(new Relacion, 'cliente_id');   
    }
}
