<?php

namespace App\Models\Coordinador;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Cliente;
use App\Models\Admin\Coordinador;

class SendByCoord extends Model
{
    protected $table = 'send_by_coord';
    protected $primaryKey = 'id';
    protected $fillable = [

        'nameOrg',
        'nameEnc',
        'ext',
        'size',
        'coordinador_id',
        'cliente_id',
        'id_user_deleted',
        'readed',
        'st_delete_coord',
        'st_delete_cliente'
    ];

    protected $appends = ['status_readed','created','delete_coord','delete_cliente'];

    //Relationships
    public function cliente(){
        return $this->belongsTo(new Cliente, 'cliente_id');
    }

    public function coordinador(){
        return $this->belongsTo(new Coordinador, 'coordinador_id');
    }

    //Accesors
    public function getCreatedAttribute(){
        return $this->created_at->format('d-m-Y H:i:s');
    }

    public function getStatusReadedAttribute(){
        switch ($this->readed) {
            case 1:
                return "<font color='#26CE3B'>SI</font>";
                break;
            case 0:
                return "<font color='#CE2626'>NO</font>";
                break;
            default:
                return "DEFAULT CASE STATUS READED";
                break;
        }
    }

    public function getDeleteCoordAttribute(){
        switch ($this->st_delete_coord) {
            case 1:
                return "<font color='#26CE3B'>SI</font>";
                break;
            case 0:
                return "<font color='#CE2626'>NO</font>";
                break;
            default:
                return "DEFAULT CASE ST DELETE COORD";
                break;
        }
    }

    public function getDeleteClienteAttribute(){
        switch ($this->st_delete_cliente) {
            case 1:
                return "<font color='#26CE3B'>SI</font>";
                break;
            case 0:
                return "<font color='#CE2626'>NO</font>";
                break;
            default:
                return "DEFAULT CASE ST DELETE COORD";
                break;
        }
    }
}
