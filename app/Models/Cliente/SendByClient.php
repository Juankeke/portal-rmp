<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Coordinador;
use App\Models\Admin\Cliente;

class SendByClient extends Model
{
    protected $table = 'send_by_client';
    protected $primaryKey = 'id';
    protected $fillable = [

        'nameOrg',
        'nameEnc',
        'ext',
        'size',
        'cliente_id',
        'coordinador_id',
        'readed',
        'st_delete_coord',
        'st_delete_cliente'
    ];

    protected $appends = ['status_readed','created'];

    //Relationships
    public function coordinador(){
        return $this->belongsTo(new Coordinador, 'coordinador_id');
    }

    public function cliente(){
        return $this->belongsTo(new Cliente, 'cliente_id');
    }

    //Accesors
    public function getCreatedAttribute(){
        return $this->created_at->format('d-m-Y H:i:s');
    }

    public function getStatusReadedAttribute(){
        switch ($this->readed) {
            case 1:
                return "<font color='#26CE3B'>SI</font>";
                break;
            case 0:
                return "<font color='#CE2626'>NO</font>";
                break;
            default:
                return "DEFAULT CASE STATUS READED";
                break;
        }
    }
}
