<?php

namespace App\Models\Tools;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dates extends Model
{
    //Obtiene si un registro ya fué modificado, ya que updated_at es igual a created_at cuando se realiza el registro
    public static function getDateUpdated( $created_at, $updated_at )
    {
        if( $created_at != $updated_at )
        {   
            return Dates::getDateName( $updated_at );            
        }
        else
        {
            return 'No se ha modificado';
        }
    }

    /*
        - Para mostrar la fecha en letra, los acentos los muestra con un caracter raro.
        - Esta funcion verifica si la fecha cae en sábado o miércoles retornamos la palabra con acento en la a, 
          de lo contrario retornamos la fecha como tal.
        - Esta función recibe la fecha a analizar para verificar si cae en sábado, miércoles o no.
    */
    public static function getDateName( $date )
    {
        setlocale( LC_ALL,"es_ES@euro" ,"es_ES", "esp" );

        // %w es para obtener la representación numérica del día de la semana, 0 (para Domingo) hasta 6 (para Sábado). 
        /*
            0-Domingo
            1-Lunes
            2-Martes
            3-Miércoles
            4-Jueves
            5-Vienres
            6-Sábado
         */

        //Vamos a obtener las primeras 3 letras del nombre del mes y del dia para enviarlas a la vista
        $day = substr( strftime( "%A", strtotime( $date )),0,3);
        $mount = substr( strftime( "%B", strtotime( $date )),0,3);

        //Verificamos si la fecha cae en '3' miércoles
        if( strftime( '%w', strtotime( $date ) ) == '3' )
        {
            return strftime( "mié %d, ".$mount." %Y, a las %H:%M:%S", strtotime( $date ) );
        }
        //Verificamos si la fecha cae en '6' sábado
        elseif( strftime( '%w', strtotime( $date ) ) == '6' )
        {
            return strftime( "sáb %d, ".$mount." %Y, a las %H:%M:%S", strtotime( $date ) );
        }
        else
        {
            return strftime( $day." %d, ".$mount." %Y, a las %H:%M:%S", strtotime( $date ) );
        } 
    }


    public static function getDateNameWithoutHours( $date )
    {
        setlocale( LC_ALL,"es_ES@euro" ,"es_ES", "esp" );

        // %w es para obtener la representación numérica del día de la semana, 0 (para Domingo) hasta 6 (para Sábado). 
        /*
            0-Domingo
            1-Lunes
            2-Martes
            3-Miércoles
            4-Jueves
            5-Vienres
            6-Sábado
         */

        //Vamos a obtener las primeras 3 letras del nombre del mes y del dia para enviarlas a la vista
        $day = substr( strftime( "%A", strtotime( $date )),0,3);
        $mount = substr( strftime( "%B", strtotime( $date )),0,3);

        //Verificamos si la fecha cae en '3' miércoles
        if( strftime( '%w', strtotime( $date ) ) == '3' )
        {
            return strftime( "mié %d, ".$mount." %Y", strtotime( $date ) );
        }
        //Verificamos si la fecha cae en '6' sábado
        elseif( strftime( '%w', strtotime( $date ) ) == '6' )
        {
            return strftime( "sáb %d, ".$mount." %Y", strtotime( $date ) );
        }
        else
        {
            return strftime( $day." %d, ".$mount." %Y", strtotime( $date ) );
        } 
    }
}
