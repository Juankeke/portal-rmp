<?php

namespace App\Models\Tools;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributesFile
{
    //Funcion para convertir de bytes a KB, MB & GB
    public static function convert_weight( $weight_bytes, $param )
    {
        if($param == 'kb')
        {
            //retorna el valor de bytes a KB
            return number_format( doubleval ( $weight_bytes / 1024 ),3,'.','' );
        }
        elseif($param == 'mb')
        {
            //retorna el valor de bytes a MB
            return number_format( doubleval ( ( $weight_bytes / 1024 ) / 1024 ),3,'.','' );
        }
        elseif($param == 'gb')
        {
            //retorna el valor de bytes a GB
            return number_format( doubleval ( ( ( $weight_bytes / 1024 ) / 1024 ) / 1024 ),3,'.','' );
        }
    }
}
