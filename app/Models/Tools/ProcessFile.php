<?php

namespace App\Models\Tools;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use App\Models\Tools\AttributesFile;

class ProcessFile
{
    public static function store_file( $file, $dir_store_file )
    {
        $answer_save_file = [];

        if(!empty($file)){

            //Obtenemos los atributos del archivo(imagen)
            $nameOrg = $file->getClientOriginalName();
            //$nameSan = SaneaString::sanear_string( $nameOrg );
            $ext = $file->getClientOriginalExtension();  
            $nameEnc = str::random( 20 ). '.' . $ext;
            $size = AttributesFile::convert_weight( $file->getSize(), 'mb' );

            $save_file = $file->move( $dir_store_file, $nameEnc );

            if($save_file){
               $answer_save_file['nameOrg'] = $nameOrg;
               //$answer_save_file['nameSan'] = $nameSan;
               $answer_save_file['nameEnc'] = $nameEnc;
               $answer_save_file['ext']     = $ext;
               $answer_save_file['size']    = $size;
               $answer_save_file['save_file'] = true;
            }
            else{
                $answer_save_file['save_file'] = false;
            }
         
        }
        else{
            $answer_save_file['nameOrg'] = '';
            //$answer_save_file['nameSan'] = '';
            $answer_save_file['nameEnc'] = '';
            $answer_save_file['ext']     = '';
            $answer_save_file['size']    = '';
            $answer_save_file['save_file'] = true;
        } 

        return $answer_save_file;
    }
}
