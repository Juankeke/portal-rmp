<?php

namespace App\Models\Tools;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeleteFiles extends model
{
    //Elimina una ruta específica
    public static function destroy_specific_url( $url_file )
    {
        $var = unlink( $url_file );

        if( $var )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    // Copea archivos de una carpeta a otra
    public static function recurse_copy( $src, $dst )
    {
        $dir = opendir( $src );
        @mkdir( $dst ); 
        
        while( false !== ( $file = readdir( $dir ) ) ) 
        { 
            if ( ( $file != '.' ) && ( $file != '..' ) ) 
            { 
                if ( is_dir( $src . '/' . $file ) ) 
                { 
                    //Si es un directorio, aplicamos recursividad y volvemos a llamar a nuestra función para copear.
                    DeleteFiles::recurse_copy( $src . '/' . $file, $dst . '/' . $file ); 
                } 
                else 
                { 
                    // Si solo es un archivo, lo copeamos
                    copy( $src . '/' . $file, $dst . '/' . $file );
                } 
            } 
        } 
        //Cerramos el archivo
        closedir( $dir );

        /*
            Después de cerrar el archivo usamos el siguiente script para eliminar las imágenes 
            en el archivo del reporte original dentro del cliente viejo.
        */
        foreach( glob( $src."/*.*" ) as $file )  
        {  
            if( is_dir( $file ) )
            {
                rmDir_rf($file);
            }
            else
            {
                unlink( $file );     
            }
        }

        rmdir( $src );
                    
        return true;
    }
    

    //Elimina archivos y directorios dentro de un directorio
    

    public static function delete_files($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!DeleteFiles::delete_files($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }
        return rmdir($dir);
    }

    //NO ESTÁ FUNCIONANDO
    public static function delete_files_old( $src )
    {
        foreach(glob( $src."/*.*" ) as $file)  
        {  
            if(is_dir($file))
            {
                //Si es un directorio, aplicamos recursividad y volvemos a llamar a nuestra función para eliminar.
                DeleteFiles::delete_files($file);
            }
            else
            {
                //Si es un solo archivo lo eliminamos.
                unlink( $file );     
            }
        }
        rmdir( $src );

        return true;
    }
}
