<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tools\DeleteFiles;

class SettingsController extends Controller
{
    public function index(){
        return view('app.admin.ajustes.index');
    }

    public function reset_folder_downloads(){
        $url = public_path().'/0000server/downloads';

        if(file_exists($url)){
            $reset = DeleteFiles::delete_files($url);

            if($reset){
                return response()->json([
                    'status_answer' => 1,
                    'title' => 'Correcto',
                    'message' => 'La carpeta de descargas se reseteó corectamente.'
                ]);
            } else{
                return response()->json([
                    'status_answer' => 2,
                    'title' => 'Error',
                    'message' => 'La carpeta de descargas no se pudo resetear.'
                ]);
            }
        } else{
            return response()->json([
                'status_answer' => 2,
                'title' => 'Error',
                'message' => 'No se ha creado la carpeta downloads.'
            ]);
        }
        

    }
}
