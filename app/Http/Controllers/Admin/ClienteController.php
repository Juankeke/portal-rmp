<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Admin\Cliente\StoreRequest as StoreCliente;
use App\Http\Requests\Admin\Cliente\UpdateRequest as UpdateCliente;
use App\Http\Requests\Admin\Cliente\UpdatePasswordRequest as UpdatePassword;

use App\Models\Admin\Cliente;
use App\Models\Admin\User;
use Hash;

class ClienteController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','admin']);
    }
    
    public function index(){
        return view('app.admin.cliente.index');
    }

    public function get(){
        $model = Cliente::with(
            [
                'user' => function( $query ){
                    $query->select('id','cliente_id','username','status');
                }
            ]
        )->get();
        return response()->json($model);
    }

    public function store(StoreCliente $request){
        $cliente = Cliente::create( array_merge( 
            $request->all(), [ 
                'password' => Hash::make( $request->password ),
                'status' => 1 //1 para usuario activo
            ] 
        ) );

        if($cliente){
            User::create([
                'role_id' => '3',//3 para rol de cliente
                'cliente_id' => $cliente->id,
                'username' => $request->username,
                'password' => Hash::make( $request->password ),
                'status' => 1 //1 para usuario activo
            ]);

            return response()->json([
                'status_answer' => 1,
                'title' => 'Correcto',
                'message' => 'El cliente y su cuenta de usuario se registró correctamente.'
            ]);
        }
    }

    public function edit($id){
        $model = Cliente::with(
            [
                'user' => function( $query ){
                    $query->select('id','cliente_id','username','status','role_id');
                }
            ]
        )->findOrFail($id);
        return response()->json($model);
    }

    public function update(UpdateCliente $request, $id){
        $cliente = Cliente::findOrFail($id);

        $cliente->update($request->all());
        $cliente->user->update(
            ['username'=>$request->username, 'status' => $request->status]
        );
       
        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información del cliente se actualizó correctamente.'
        ]);
    }

    public function update_pass(UpdatePassword $request, $id){
        $cliente = Cliente::findOrFail($id);
        $cliente->user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'status_answer' => 1,
            'title' => 'Contraseña actualizada',
            'message' => 'La contraseña de - '.$cliente->razon_social.' - se actualizó correctamente.'
        ]);
    }
}
