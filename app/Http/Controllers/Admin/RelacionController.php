<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Cliente;
use App\Models\Admin\Relacion;

use App\Models\Admin\Coordinador;

class RelacionController extends Controller
{
    public function __construct(Request $request){
        $this->middleware(['user_active','admin']);
        $this->coordinador = Coordinador::findOrFail($request->id);
    }

    public function index(){
        $array = ['coordinador'=>$this->coordinador];
        return view('app.admin.relacion.index')->with($array);
    }

    public function get_clients(){
        $clientes = Cliente::select('id','razon_social','rfc')->get();
        return response()->json($clientes);
    }

    public function get_relations(){
        $colle = $this->coordinador->relaciones()->with('cliente')->get();
        return response()->json($colle);
    }

    public function store(Request $request){
        $data = json_decode($_POST['clientes']);

        foreach($data as $item){
            $exist = Relacion::where([
                ['coordinador_id',$this->coordinador->id],
                ['cliente_id',$item]
            ])->get();

            if($exist->isEmpty()){
                $create = Relacion::create([
                    'coordinador_id' => $this->coordinador->id,
                    'cliente_id' => $item
                ]);
            } else{
                //Ya existe, no procede el registro.
            }
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se guardó correctamente.'
        ]);
    }

    public function destroy($id, $id_rel){
        Relacion::findOrFail($id_rel)->delete();

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La relación se eliminó correctamente.'
        ]);   
    }
}
