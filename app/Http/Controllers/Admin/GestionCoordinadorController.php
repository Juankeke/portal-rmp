<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Coordinador\SendByCoord;
use App\Models\Tools\DeleteFiles;

class GestionCoordinadorController extends Controller
{
    public function __construct(Request $request){
        $this->middleware(['user_active','admin']);
    }

    public function enviados(){
        return view('app.admin.coordinador.enviados.index');
    }

    public function get_enviados(){
        $files = SendByCoord::with('cliente','coordinador')->get();
        return response()->json($files);
    }

    public function rec_eliminado_coord($id){
        $file = SendByCoord::findOrFail($id);
        $file->update(['st_delete_coord'=>0]);
        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'El archivo se recuperó correctamente para el coordinador.'
        ]);
    }

    public function rec_eliminado_cliente($id){
        $file = SendByCoord::findOrFail($id);
        $file->update(['st_delete_cliente'=>0]);
        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'El archivo se recuperó correctamente para el cliente.'
        ]);
    }

    public function eliminar_enviados(Request $request){
        $data = json_decode($_POST['files']);

        foreach($data as $item){
            $file = SendByCoord::findOrFail($item);
            $__urlDestroy = public_path().'/0000server/send_coordinador/'.$file->nameEnc;

            if(file_exists($__urlDestroy)){
                DeleteFiles::destroy_specific_url($__urlDestroy);
            } 
            $file->delete();
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se eliminó correctamente.'
        ]);
    }
}
