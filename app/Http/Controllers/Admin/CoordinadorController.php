<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Admin\Coordinador\StoreRequest as StoreCoordinador;
use App\Http\Requests\Admin\Coordinador\UpdateRequest as UpdateCoordinador;
use App\Http\Requests\Admin\Coordinador\UpdatePasswordRequest as UpdatePassword;

use App\Models\Admin\Coordinador;
use App\Models\Admin\User;
use Hash;

class CoordinadorController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','admin']);
    }
    
    public function index(){
        return view('app.admin.coordinador.index');
    }

    public function get(){
        $model = Coordinador::with(
            [
                'user' => function( $query ){
                    $query->select('id','coordinador_id','username','status');
                }
            ]
        )->get();
        return response()->json($model);
    }

    public function store(StoreCoordinador $request){
        $coordinador = Coordinador::create( array_merge( 
            $request->all(), [ 
                'password' => Hash::make( $request->password ),
                'status' => 1 //1 para usuario activo
            ] 
        ) );

        if($coordinador){
            User::create([
                'role_id' => '2',//2 para rol de coordinador
                'coordinador_id' => $coordinador->id,
                'username' => $request->username,
                'password' => Hash::make( $request->password ),
                'status' => 1 //1 para usuario activo
            ]);

            return response()->json([
                'status_answer' => 1,
                'title' => 'Correcto',
                'message' => 'El coordinador y su cuenta de usuario se registró correctamente.'
            ]);
        }
    }

    public function edit($id){
        $model = Coordinador::with(
            [
                'user' => function( $query ){
                    $query->select('id','coordinador_id','username','status','role_id');
                }
            ]
        )->findOrFail($id);
        return response()->json($model);
    }

    public function update(UpdateCoordinador $request, $id){
        $coord = Coordinador::findOrFail($id);

        $coord->update($request->all());
        $coord->user->update(
            ['username'=>$request->username, 'status' => $request->status]
        );
       
        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información del coordinador se actualizó correctamente.'
        ]);
    }

    public function update_pass(UpdatePassword $request, $id){
        $coordinador = Coordinador::findOrFail($id);
        $coordinador->user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'status_answer' => 1,
            'title' => 'Contraseña actualizada',
            'message' => 'La contraseña de - '.$coordinador->full_name.' - se actualizó correctamente.'
        ]);
    }
}
