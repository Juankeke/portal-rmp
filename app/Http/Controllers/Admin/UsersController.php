<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Admin\User\StoreRequest as StoreUser;
use App\Http\Requests\Admin\user\UpdateRequest as UpdateUser;
use App\Http\Requests\Admin\user\UpdatePasswordRequest as UpdatePassword;

use App\Models\Admin\User;
use Hash;

class UsersController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','admin']);
    }
    
    public function index(){
        return view('app.admin.users.index');
    }

    public function get_users(){
        $colle = User::with('role')->where('role_id',1)->get();
        return response()->json(['data' => $colle]);
    }

    public function store(StoreUser $request){
        $user_store = User::create( array_merge( 
            $request->all(), [ 
                'password' => Hash::make( $request->password ),
                'role_id' => 1,//1 para rol de ADMINISTRADOR
                'status' => 1 //1 para usuario activo
            ] 
        ) );

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La cuenta de usuario se registró correctamente.'
        ]);
    }

    public function edit($id){
        $user = User::select(
            'id',
            'paterno',
            'materno',
            'nombre',
            'username',
            'status'
        )->findOrFail($id);
        return response()->json($user);
    }

    public function update(UpdateUser $request, $id){
        $object = User::findOrFail($id);        
        $object_update = $object->fill($request->all());

        if($object_update->isDirty()){
            $object->update($request->all());

            return response()->json([
                'status_answer' => 1,
                'title' => 'Correcto',
                'message' => 'La cuenta de usuario se modificó correctamente.'
            ]);
        } else{
            return response()->json([
                'status_answer' => 0,
                'title' => 'Información',
                'message' => 'No se realizó ningún cambio.'
            ]);
        }
    }

    public function update_password(UpdatePassword $request, $id){
        $user = User::findOrFail($id);
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status_answer' => 1,
            'title' => 'Contraseña actualizada',
            'message' => 'La contraseña de - '.$user->full_name.' - se actualizó correctamente.'
        ]);
    }
}
