<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Request $request ){
        $this->middleware(['auth','user_active']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        switch (auth()->user()->role->name) {
            case 'ADMINISTRADOR':
                return view('app.home.admin');
                break;
            case 'COORDINADOR':
                return redirect()->to('coordinador/received_files');
                break;
            case 'CLIENTE':
                return redirect()->to('cliente/received_files');
                break;
            default:
                return 0;
                break;
        }        
    }
}
