<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Admin\Relacion;
use App\Models\Admin\Coordinador;
use App\Models\Cliente\SendByClient;
use App\Models\Coordinador\SendByCoord;

use App\Models\Tools\ProcessFile;
use App\Models\Tools\DeleteFiles;
use App\Models\Tools\Dates;
use App\Mail\NotifyMailCoord;
use Storage;
use Madzipper;
use Mail;


class FilesController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','client']);
    }

    public function select_coord(){
        return view('app.cliente.send.select_coord');
    }

    public function get_coords(){
        $colle = Relacion::where('coordinador_id',auth()->user()->cliente_id)->with('coordinador')->get();
        return response()->json($colle);
    }

    public function view_send($id){
        $coordinador = Coordinador::findOrFail($id);
        Return view('app.cliente.send.send',compact('coordinador'));
    }

    public function send(Request $request, $id){
        $coordinador = Coordinador::findOrFail($id);
        $__urlSave = public_path().'/0000server/send_cliente';

        if(file_exists($__urlSave)){
            $answer_save_file = ProcessFile::store_file(
                $request->file('file'),
                $__urlSave
            );

            if($answer_save_file['save_file'] == true )
            {
                $file = new SendByClient;
                $file->cliente_id = auth()->user()->cliente_id;
                $file->coordinador_id = $id;
                $file->nameOrg = $answer_save_file[ 'nameOrg' ];                
                $file->nameEnc = $answer_save_file['nameEnc'];
                $file->ext = $answer_save_file['ext'];
                $file->size = $answer_save_file['size'];
                $file->readed = 0; //0 para no leído
                $file->st_delete_coord = 0;
                $file->st_delete_cliente = 0;
                $file->save();

                if($file->save()){
                    $details = [
                        'coordinador' => $coordinador->getFullNameAttribute(),
                        'archivo' => $file->nameOrg,
                        'fecha' => Dates::getDateName($file->created_at)
                    ];
                    Mail::to('sistemas@rmp.mx')->cc('sistemas@rmp.mx')->send(new NotifyMailCoord($details));
                }
            }
        }
    }


    //ENVIADOS
    public function files_sended(){
        return view('app.cliente.enviados.index');
    }

    public function get_files_sended(){
        $files = SendByClient::where(
            [
                ['cliente_id',auth()->user()->cliente_id],
                ['st_delete_cliente',0]
            ]
        )->with('coordinador')->get();
        return response()->json($files);
    }

    public function delete_files_sended(Request $request){
        $data = json_decode($_POST['files']);

        foreach($data as $item){
            $file = SendByClient::findOrFail($item);
            
            /*$__urlDestroy = public_path().'/0000server/send_cliente/'.$file->nameEnc;

            if(file_exists($__urlDestroy)){
                DeleteFiles::destroy_specific_url($__urlDestroy);
            } 
            */
            $file->update(['st_delete_cliente'=>1]);
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se guardó correctamente.'
        ]);
    }

    public function download_files_sended(Request $request){
        $data = json_decode($_POST['files']);

        //Creamos un directorio random en la carpeta de send_coordinador
        $nameEnc = str::random(10);
        Storage::makeDirectory('downloads/'.$nameEnc.'_Portal');

        $src = public_path().'/0000server/send_cliente';//Carpeta origen
        $dst = public_path().'/0000server/downloads/'.$nameEnc.'_Portal';//Carpeta destino

        //$dir = opendir($src);
        //@mkdir($dst); 

        foreach($data as $item){
            $item = SendByClient::findOrFail($item);
            //Copeamos cada uno a la carpeta temporal
            copy($src . '/' . $item->nameEnc, $dst . '/' . $item->nameOrg);
        }

        $files = glob('0000server/downloads/'.$nameEnc.'_Portal');//Preparamos los archivos
        Madzipper::make('0000server/downloads/'.$nameEnc.'_Portal.zip')->add($files)->close();//Agregamos al ZIP

        $__urlDownload = public_path().'/0000server/downloads/'.$nameEnc.'_Portal.zip';
        return response()->download($__urlDownload);

        //Después de retornar la descarga eliminamos las carpetas normal y compresa
        //DeleteFiles::delete_files(public_path().'/0000server/downloads'.$nameEnc.'_Portal');
        //DeleteFiles::destroy_specific_url(public_path().'/0000serverdownloads/'.$nameEnc.'_Portal.zip');
    }

    public function download_file($id){
        $file = SendByClient::findOrFail($id);

        $__urlDownload = public_path().'/0000server/send_cliente/'.$file->nameEnc;

        if(file_exists($__urlDownload)){
            return response()->download($__urlDownload, $file->nameOrg);
        }
    }

    //RECIBIDOS
    public function received_files(){
        return view('app.cliente.recibidos.index');
    }

    public function get_received_files(){
        $colle = SendByCoord::where(
            [
                ['cliente_id',auth()->user()->cliente_id],
                ['st_delete_cliente',0]
            ]
        )->with('coordinador')->get();
        return response()->json($colle);
    }

    public function delete_received_files(Request $request){
        $data = json_decode($_POST['files']);

        foreach($data as $item){
            $file = SendByCoord::findOrFail($item);

            /*
            $__urlDestroy = public_path().'/0000server/send_coordinador/'.$file->nameEnc;

            if(file_exists($__urlDestroy)){
                DeleteFiles::destroy_specific_url($__urlDestroy);
            } 
            */
            $file->update(['st_delete_cliente'=>1]);
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se eliminó correctamente.'
        ]);
    }

    public function download_received_files(Request $request){
        $data = json_decode($_POST['files']);

        //Creamos un directorio random en la carpeta de send_coordinador
        $nameEnc = str::random(10);
        Storage::makeDirectory('downloads/'.$nameEnc.'_Portal');

        $src = public_path().'/0000server/send_coordinador';//Carpeta origen
        $dst = public_path().'/0000server/downloads/'.$nameEnc.'_Portal';//Carpeta destino

        //$dir = opendir($src);
        //@mkdir($dst); 

        foreach($data as $item){
            $item = SendByCoord::findOrFail($item);
            //Copeamos cada uno a la carpeta temporal
            copy($src . '/' . $item->nameEnc, $dst . '/' . $item->nameOrg);
            $item->update(['readed'=>1]);
        }

        $files = glob('0000server/downloads/'.$nameEnc.'_Portal');//Preparamos los archivos
        Madzipper::make('0000server/downloads/'.$nameEnc.'_Portal.zip')->add($files)->close();//Agregamos al ZIP

        $__urlDownload = public_path().'/0000server/downloads/'.$nameEnc.'_Portal.zip';
        return response()->download($__urlDownload);

        //Después de retornar la descarga eliminamos las carpetas normal y compresa
        //DeleteFiles::delete_files(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal');
        //DeleteFiles::destroy_specific_url(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal.zip');
    }

    public function download_file_received($id){
        $file = SendByCoord::findOrFail($id);

        $__urlDownload = public_path().'/0000server/send_coordinador/'.$file->nameEnc;

        if(file_exists($__urlDownload)){
            $file->update(['readed'=>1]);
            return response()->download($__urlDownload, $file->nameOrg);
        }
    }
}
