<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Cliente\UpdatePasswordRequest;

use App\Models\Admin\User;
use Hash;

class AjustesController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','client']);
    }
    
    public function index(){
        return view('app.cliente.ajustes.index');
    }

    public function update_password(UpdatePasswordRequest $request){
        if (Hash::check($request->old_password, auth()->user()->password)){
            User::findOrFail(auth()->user()->id)->update([
                'password' => Hash::make($request->password)
            ]);

            return response()->json([
                'status_answer' => 1,
                'title' => 'Correcto',
                'message' => 'Tu contraseña se actualizó correctamente.'
            ]);

        }else{
            return response()->json([
                'status_answer' => 2,
                'title' => 'Error',
                'message' => 'La contraseña anterior no coincide.'
            ]);
        }
    }
}
