<?php

namespace App\Http\Controllers\Coordinador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Admin\Relacion;
use App\Models\Admin\Cliente;
use App\Models\Coordinador\SendByCoord;
use App\Models\Cliente\SendByClient;

use App\Models\Tools\ProcessFile;
use App\Models\Tools\DeleteFiles;
use App\Models\Tools\Dates;
use App\Mail\NotifyMailClient;
use Storage;
use Madzipper;
use Mail;

class FilesController extends Controller
{
    public function __construct( Request $request ){
        $this->middleware(['user_active','coord']);
    }

    public function select_client(){
        return view('app.coordinador.send.select_cliente');
    }

    public function get_clients(){
        $clients = Relacion::where('coordinador_id',auth()->user()->coordinador_id)->with('cliente')->get();
        return response()->json($clients);
    }

    public function view_send($id){
        $cliente = Cliente::findOrFail($id);
        Return view('app.coordinador.send.send',compact('cliente'));
    }

    public function send(Request $request, $id){
        $cliente = Cliente::findOrFail($id);
        $__urlSave = public_path().'/0000server/send_coordinador';

        if(file_exists($__urlSave)){
            $answer_save_file = ProcessFile::store_file(
                $request->file('file'),
                $__urlSave
            );

            if($answer_save_file['save_file'] == true )
            {
                $file = new SendByCoord;
                $file->coordinador_id = auth()->user()->coordinador_id;
                $file->cliente_id = $id;
                $file->nameOrg = $answer_save_file[ 'nameOrg' ];                
                $file->nameEnc = $answer_save_file['nameEnc'];
                $file->ext = $answer_save_file['ext'];
                $file->size = $answer_save_file['size'];
                $file->readed = 0; //0 para no leído
                $file->st_delete_coord = 0;
                $file->st_delete_cliente = 0;
                $file->save();

                if($file->save()){
                    $details = [
                        'cliente' => $cliente->razon_social,
                        'archivo' => $file->nameOrg,
                        'fecha' => Dates::getDateName($file->created_at)
                    ];
                    Mail::to('sistemas@rmp.mx')->cc('sistemas@rmp.mx')->send(new NotifyMailClient($details));
                }
            }
        }
    }

    //ENVIADOS
    public function files_sended(){
        return view('app.coordinador.enviados.index');
    }

    public function get_files_sended(){
        $files = SendByCoord::where(
            [
                ['coordinador_id',auth()->user()->coordinador_id],
                ['st_delete_coord',0]
            ]
        )->with('cliente')->get();
        return response()->json($files);
    }

    public function delete_files_sended(Request $request){
        $data = json_decode($_POST['files']);

        foreach($data as $item){
            $file = SendByCoord::findOrFail($item);

            /*
            $__urlDestroy = public_path().'/0000server/send_coordinador/'.$file->nameEnc;

            if(file_exists($__urlDestroy)){
                DeleteFiles::destroy_specific_url($__urlDestroy);
            } 

            $file->update(['id_user_deleted'=>auth()->user()->coordinador_id]);
            $file->delete();
            */
            $file->update(['st_delete_coord'=>1]);
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se eliminó correctamente.'
        ]);
    }

    public function download_files_sended(Request $request){
        $data = json_decode($_POST['files']);

        //Creamos un directorio random en la carpeta de send_coordinador
        $nameEnc = str::random(10);
        Storage::makeDirectory('downloads/'.$nameEnc.'_Portal');

        $src = public_path().'/0000server/send_coordinador';//Carpeta origen
        $dst = public_path().'/0000server/downloads/'.$nameEnc.'_Portal';//Carpeta destino

        //$dir = opendir($src);
        //@mkdir($dst); 

        foreach($data as $item){
            $item = SendByCoord::findOrFail($item);
            //Copeamos cada uno a la carpeta temporal
            copy($src . '/' . $item->nameEnc, $dst . '/' . $item->nameOrg);
        }

        $files = glob('0000server/downloads/'.$nameEnc.'_Portal');//Preparamos los archivos
        Madzipper::make('0000server/downloads/'.$nameEnc.'_Portal.zip')->add($files)->close();//Agregamos al ZIP

        $__urlDownload = public_path().'/0000server/downloads/'.$nameEnc.'_Portal.zip';
        return response()->download($__urlDownload);

        //Después de retornar la descarga eliminamos las carpetas normal y compresa
        //DeleteFiles::delete_files(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal');
        //DeleteFiles::destroy_specific_url(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal.zip');
    }

    public function download_file($id){
        $file = SendByCoord::findOrFail($id);

        $__urlDownload = public_path().'/0000server/send_coordinador/'.$file->nameEnc;

        if(file_exists($__urlDownload)){
            return response()->download($__urlDownload, $file->nameOrg);
        }
    }

    //papelera
    /*
    public function sent_bin(){
        return view('app.coordinador.enviados.papelera.index');
    }

    public function get_sent_bin(){
        $files = SendByCoord::where(
            [
                ['coordinador_id',auth()->user()->coordinador_id],
                ['st_delete_coord',1]
            ]
        )->with('cliente')->get();
        return response()->json($files);
    }

    public function recover_sent(){
        $data = json_decode($_POST['files']);
        foreach($data as $item){
            $file = SendByCoord::findOrFail($item);
            $file->update(['st_delete_coord'=>0]);
        }
        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se eliminó correctamente.'
        ]);
    }

    */



    //RECIBIDOS
    public function received_files(){
        return view('app.coordinador.recibidos.index');
    }

    public function get_received_files(){
        $colle = SendByClient::where(
            [
                ['coordinador_id',auth()->user()->coordinador_id],
                ['st_delete_coord',0]
            ]
        )->with('cliente')->get();
        return response()->json($colle);
    }

    public function delete_received_files(Request $request){
        $data = json_decode($_POST['files']);

        foreach($data as $item){
            $file = SendByClient::findOrFail($item);
            /*
            $__urlDestroy = public_path().'/0000server/send_cliente/'.$file->nameEnc;

            if(file_exists($__urlDestroy)){
                DeleteFiles::destroy_specific_url($__urlDestroy);
            } 

            $file->update(['id_user_deleted'=>auth()->user()->coordinador_id]);
            $file->delete();
            */
            $file->update(['st_delete_coord'=>1]);
        }

        return response()->json([
            'status_answer' => 1,
            'title' => 'Correcto',
            'message' => 'La información se eliminó correctamente.'
        ]);
    }

    public function download_received_files(Request $request){
        $data = json_decode($_POST['files']);

        //Creamos un directorio random en la carpeta de send_coordinador
        $nameEnc = str::random(10);
        Storage::makeDirectory('downloads/'.$nameEnc.'_Portal');

        $src = public_path().'/0000server/send_cliente';//Carpeta origen
        $dst = public_path().'/0000server/downloads/'.$nameEnc.'_Portal';//Carpeta destino

        //$dir = opendir($src);
        //@mkdir($dst); 

        foreach($data as $item){
            $item = SendByClient::findOrFail($item);
            //Copeamos cada uno a la carpeta temporal
            copy($src . '/' . $item->nameEnc, $dst . '/' . $item->nameOrg);
            $item->update(['readed'=>1]);
        }

        $files = glob('0000server/downloads/'.$nameEnc.'_Portal');//Preparamos los archivos
        Madzipper::make('0000server/downloads/'.$nameEnc.'_Portal.zip')->add($files)->close();//Agregamos al ZIP

        $__urlDownload = public_path().'/0000server/downloads/'.$nameEnc.'_Portal.zip';
        return response()->download($__urlDownload);

        //Después de retornar la descarga eliminamos las carpetas normal y compresa
        //DeleteFiles::delete_files(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal');
        //DeleteFiles::destroy_specific_url(public_path().'/0000server/send_coordinador/'.$nameEnc.'_Portal.zip');
    }


    public function download_file_received($id){
        $file = SendByClient::findOrFail($id);

        $__urlDownload = public_path().'/0000server/send_cliente/'.$file->nameEnc;

        if(file_exists($__urlDownload)){
            $file->update(['readed'=>1]);
            return response()->download($__urlDownload, $file->nameOrg);
        }
    }


}
