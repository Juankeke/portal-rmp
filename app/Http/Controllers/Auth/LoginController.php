<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\PostRequest as PostLogin;

use App\Models\Admin\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(){
        return 'username';
    }

    public function showLoginForm(){
        return view('master.login');
    }

    public function postLogin(PostLogin $request){       
        $user = User::where( 'username',$request->username )->first();

        if($user!= null){
            switch($user->status) {
                case '1'://1 para usuarios activos
                    return $this->login($request);
                    break;
                case '0': //0 para usuarios inactivos
                    return redirect()->back(); //Podemos retornar a la vista de compra de licencias
                    break;
                default:
                    return redirect()->back();
                    break;
            }
        } else{
            return $this->login( $request );
        }      
    }

    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('login');
    }
}
