<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required | min:8 | confirmed',
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'La contraseña anterior es obligatoria',
            'password.min' => 'Mínimo 8 dígitos para la contraseña',
            'password.required' => 'La contraseña y confirmación son obligatorias',
            'password.confirmed' => 'Las contraseñas no coinciden, vuelve a escribirlas',
        ];
    }
}
