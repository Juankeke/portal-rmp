<?php

namespace App\Http\Requests\Admin\Coordinador;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'paterno' => 'required',
            'materno' => 'required',
            'nombre' => 'required',
            'email' => 'required | email',
            'username' => 'required | min:6 | unique:users',
            'password' => 'required | min:8 | confirmed'
        ];
    }

    public function messages(){
        return [
            'required' => '*Campo obligatorio',
            'email' => 'Debe ser de tipo email',
            'username.min' => 'Mínimo 6 caractéres',
            'username.unique' => 'Este nombre de usuario no está disponible, intenta con otro',
            'password.min' => 'Mínimo 8 caracteres',
            'password.required' => 'La contraseña y confirmación son obligatorias',
            'password.confirmed' => 'Las contraseñas no coinciden, vuelve a escribirlas'
        ];
    }
}
