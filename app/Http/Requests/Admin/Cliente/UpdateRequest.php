<?php

namespace App\Http\Requests\Admin\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'razon_social' => 'required | unique:cliente,razon_social,'.$this->id.',id',
            'rfc' => 'required | between: 12,13 | alpha_num | unique:cliente,rfc,'.$this->id.',id',
            'email' => 'required | email | unique:cliente,email,'.$this->id.',id',
            'username' => 'required | min:6 | unique:users,username,'.$this->id.',cliente_id'
        ];
    }

    public function messages(){
        return [
            'required' => '*Campo obligatorio',
            'razon_social.unique' => 'Esta razón social ya está registrada',
            'rfc.between' => 'Mínimo 12 y máximo 13 caracteres',
            'rfc.alpha_num' => 'Solo caracteres entre [A-Z y 0-9]',
            'rfc.unique' => 'Este RFC ya está registrado',
            'email' => 'Debe ser de tipo email',
            'emai.unique' => 'Este email ya está registrado',
            'username' => 'Mínimo 6 caracteres',
            'username.unique' => 'Este nombre de usuario ya está registrado'
        ];
    }
}
