<?php

namespace App\Http\Requests\Admin\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'razon_social' => 'required | unique:cliente',
            'rfc' => 'required | between: 12,13 | alpha_num | unique:cliente',
            'email' => 'required | email | unique:cliente',
            'username' => 'required | min:6 | unique:users',
            'password' => 'required | min:8 | confirmed'
        ];
    }

    public function messages(){
        return [
            'required' => '*Campo obligatorio',
            'razon_social.unique' => 'Esta razón social ya está registrada',
            'rfc.between' => 'Mínimo 12 y máximo 13 caracteres',
            'rfc.alpha_num' => 'Solo caracteres entre [A-Z y 0-9]',
            'rfc.unique' => 'Este RFC ya está registrado',
            'email' => 'Debe ser de tipo email',
            'email.unique' => 'Este email ya está registrado.',
            'username' => 'Mínimo 6 caracteres',
            'username.unique' => 'Este nombre de usuario ya está registrado',
            'password.min' => 'Mínimo 8 caracteres',
            'password.required' => 'La contraseña y confirmación son obligatorias',
            'password.confirmed' => 'Las contraseñas no coinciden, vuelve a escribirlas'
        ];
    }
}
