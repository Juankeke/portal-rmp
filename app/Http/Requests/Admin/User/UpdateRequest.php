<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
   
    public function rules()
    {
        return [
            'paterno' => 'required',
            'materno' => 'required',
            'nombre' => 'required',
            'username' => 'required | min:6 | unique:users,username,'.$this->id.',id'
        ];
    }

    public function messages()
    {
        return [
            'required' => '*Obligatorio',
            'username.min' => 'Mínimo 6 caractéres',
            'username.unique' => 'Este nombre de usuario no está disponible, intenta con otro'            
        ];
    }
}
