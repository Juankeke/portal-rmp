<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [

            'username.required' => 'El <strong>nombre de usuario</strong> es requerido.',
            'password.required' => 'La <strong>contraseña</strong> es requerida.',

        ];
    }
}
