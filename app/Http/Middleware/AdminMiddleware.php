<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        //Primero verificamos que el usuario este logueado
        if(Auth::guard($guard)->check()){
            //Vamos a bloquear a todo usuario que no sea administrador y que quiera entrar a una ruta protegida por este middleware
            if(Auth::user()->role->name != 'ADMINISTRADOR'){
                return redirect()->to('401');
            }
        } else{
            return redirect()->to('login');
        }

        return $next($request);
    }
}
