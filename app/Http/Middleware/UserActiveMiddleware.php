<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        //Primero verificamos que el usuario este logueado
        if(Auth::guard($guard)->check()){
            //Vamos a bloquear a todo usuario que no esté activo
            if(Auth::user()->status != 1){
                return redirect()->to('401_inactive');
            }
        } else{
            return redirect()->to( 'login' );
        }

        return $next($request);
    }
}
