<div class="modal fade forget-modal" tabindex="-1" id="modal_notice_of_privacy" role="dialog" aria-labelledby="myForgetModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <!--<span aria-hidden="true">×</span>-->
                    <span class="sr-only">Close</span>
                </button>
                <h5 class="modal-title text-center"><strong>AVISO DE PRIVACIDAD</strong></h5>
            </div>
            <div class="modal-body">
                <p align="justify">
                    <font size="2">
                    <strong><font color="#005A8A">Ramírez Medellín, S.C.</font></strong>, con domicilio en Francisco Peña No. 245, Col. Jardín, en la ciudad de San Luis Potosí, S.L.P., México, con fundamento legal en lo dispuesto por los artículos 8, 12, 13, 15, 16, 17, 22, 23, 24, 25 y demás aplicables a la Ley Federal de Protección de Datos Personales, así como los de su reglamento, en Posesión de los Particulares, informa que:
                    </font>
                </p>
                <p align="justify">
                    <font size="2">
                    <strong>1.</strong> La finalidad de toda información que pase o que tenga acceso <strong>Ramírez Medellín, S.C.</strong>, así como por el Portal de la firma, es de uso exclusivo entre el cliente y el personal con el que está siendo asesorado, todos los archivos son confidenciales, <strong>Ramírez Medellín, S.C.</strong> no tiene ningún interés en divulgar dicha información, el Portal es para facilitar la comunicación entre los mencionados.
                    </font>
                </p>
                <p align="justify">
                    <font size="2">
                    <strong>2.</strong> El medio de comunicación es de estricto uso profesional, el Portal es de uso exclusivo entre clientes y personal  que forme parte de <strong>Ramírez Medellín, S.C.</strong>, estos serán los responsables de lo que transfieran mediante el empleo del Portal, ninguna otra persona tendrá acceso a las cuentas personales, ni a los datos de transferencia.
                    </font>
                </p>
                <p align="justify">
                    <font size="2">
                    <strong>3.</strong> El usuario podrá ejercer los derechos de acceso, rectificación, cancelación y oposición previstos en la Ley en el momento que considere necesario, de igual manera el usuario tiene derecho al acceso de los datos personales que se tengan en posesión, en tal caso, deberá de dar aviso de forma inmediata al administrador del Portal, para que este haga las operaciones necesarias.
                    </font>
                </p>   
                <p align="justify">
                    <font size="2">
                    <strong>4.</strong> En el momento en que ya no se necesite de la cuenta, la persona debe comunicarse con el administrador del Portal para que la cancele, solo éste podría hacerlo por seguridad del Portal. La información alojada durará tres meses a partir del dia que se cargue la misma, después de ese tiempo se eliminará, y no habrá forma de recuperar dicha información.
                    </font>
                </p>    
                <p align="justify">
                    <font size="2">
                    <strong>5.</strong> Los usuarios de las cuentas serán los responsables de que la información que se cargue al Portal esté libre de virus, ya que esto podría afectar su funcionamiento y de igual manera poner en riesgo la seguridad de los datos almacenados en el Portal.  
                    </font>
                </p>
                <p align="justify">
                    <font size="2">
                    Los datos personales a los que se tenga acceso, son para el uso exclusivo de <strong>Ramírez Medellín, S.C.</strong>, los cuales serán para proveer los servicios que ha solicitado y que sean necesarios para la realización del objeto.
                    </font>
                </p>  
                <p align="justify">
                    <font size="2">
                    Le informamos que sus datos personales no serán transferidos, a menos que así lo disponga una ley u obligación contractual, en este caso se le dará aviso expreso de la situación y de los datos que se encuentran en poder de <strong>Ramírez Medellín, S.C.</strong>
                    </font>
                </p> 
                <p align="justify">
                    <font size="2">
                    <strong><font color="#005A8A">Ramírez Medellín, S.C.</font></strong> no es responsable del mal uso que le puedan dar a la plataforma, ni alos datos contenidos y que por causas ajenas a este se perdieran o borraran en el momento previsto.
                    </font>
                </p>        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->