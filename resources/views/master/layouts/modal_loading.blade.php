<div class="modal fade bs-example-modal-sm" id="modal_loading" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div style="margin: 40px;">
                    <center>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw" id="loading_action"></i>
                    </center>
                </div>
            </div>                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


