<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="{{{ asset('favicon.ico') }}}">
	<title>Portal RMP</title>

	{!! Html::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback') !!}
	{!! Html::style('plugins/fontawesome-free/css/all.min.css') !!}
	{!! Html::style('dist/css/custom_style.css') !!}
	{!! Html::style('dist/css/adminlte.css') !!}
</head>

<body class="hold-transition login-page">

	<div style="margin-bottom: 50px;">
		{!! Html::image('images/rm.png',null,['width'=>'400', 'height'=> 'auto']) !!}
	</div>
	
	<div class="login-box">
  		<!-- /.login-logo -->
  		<div class="card card-outline card-primary">
    		<div class="card-header text-center">
      			<a class="h4"><b>Portal</b> | Login</a>
    		</div>
    		<div class="card-body">

      			{!! Form::open( [ 'url'=>'login_post', 'method'=>'POST', 'role'=>'form' ] ) !!}
        			<div class="input-group mb-3">
        				<div class="input-group-append">
            				<div class="input-group-text">
	              				<span class="fas fa-user"></span>
            				</div>
          				</div>
          				{!! Form::text(
	                        'username', 
	                        null, 
	                        [
	                            'class' => 'form-control',
	                            'autocomplete'=>'off',
	                            'placeholder' => 'Usuario',                                               
	                            'id' => 'username'
	                        ]
	                    ) !!} 
        			</div>
        			@if( $errors->has('username') )          
	                    @foreach($errors->get('username') as $error )   
	                        <span class="input-error">{!! $error !!}</span></br>
	                    @endforeach
	                @endif

        			<div class="input-group mb-3">
        				<div class="input-group-append">
            				<div class="input-group-text">
              					<span class="fas fa-lock"></span>
            				</div>
          				</div>
          				<input type="password" name="password" class="form-control" placeholder="Contraseña" id="password">
        			</div>
        			@if( $errors->has('password') )          
	                    @foreach($errors->get('password') as $error )   
	                        <span class="input-error">{!! $error !!}</span></br>
	                    @endforeach
	                @endif
                
        			<div class="row">
          				<div class="col-8">
            				<div class="icheck-primary">
              					<input type="checkbox" id="remember">
              					<label for="remember">
                					Remember Me
              					</label>
            				</div>
          				</div>
          				<!-- /.col -->
          				<div class="col-4">
            				<button type="submit" class="btn btn-success btn-block">Entrar</button>
          				</div>
          				<!-- /.col -->
        			</div>
      			{!! Form::close() !!}
    		</div>
    		<!-- /.card-body -->
  		</div>
  		<!-- /.card -->
	</div>
	<!-- /.login-box -->

	@include('master.layouts.notice_of_privacy')

	<br>
	<center>
		<button class="btn btn-primary" data-toggle="modal" data-target="#modal_notice_of_privacy">Aviso de privacidad</button>
	</center>


{!! Html::script('plugins/jquery/jquery.min.js') !!}
{!! Html::script('plugins/bootstrap/js/bootstrap.bundle.min.js') !!}
{!! Html::script('dist/js/adminlte.min.js') !!}

</body>
</html>