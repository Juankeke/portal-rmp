<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" type="image/png" href="{{{ asset('favicon.ico') }}}">
    	  <title>Portal</title>

      	{!! Html::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback') !!}
      	{!! Html::style('plugins/fontawesome-free/css/all.min.css') !!}
        {!! Html::style('plugins/toastr/toastr.min.css') !!}
        {!! Html::style('plugins/sweetalert/sweetalert.css') !!}
        {!! Html::style('plugins/datatables/css/jquery.dataTables.css') !!}
        {!! Html::style('plugins/datatables/fixedColumns/css/fixedColumns.dataTables.css') !!}
        {!! Html::style('plugins/dropzone/dropzone.css') !!}
      	{!! Html::style('dist/css/adminlte.min.css') !!}
        {!! Html::style('dist/css/custom_style.css') !!}

        @section('head')
        @show

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">

  	<div class="wrapper">
    	  <!-- Navbar -->
    		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
      	    <!-- Left navbar links -->
      		  <ul class="navbar-nav">
          			<li class="nav-item">
            			  <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          			</li>
          			<li class="nav-item d-none d-sm-inline-block">
            			<a href="{!!route('coord.received_files')!!}" class="nav-link">
                        Panel coordinador | 
                        <strong>{!! auth()->user()->coordinador->getFullNameAttribute() !!}</a></strong>
          			</li>
      		  </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{!!route('coord.received_files')!!}" class="brand-link">
                <!--
                <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                -->
                <span class="brand-text font-weight-light">RMP</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="{!! route('coord.select_client') !!}" class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    Hacer un envío
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('coord.received_files') !!}" class="nav-link">
                                <i class="nav-icon fas fa-file-import"></i>
                                <p>
                                    Recibidos
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('coord.files_sended') !!}" class="nav-link">
                                <i class="nav-icon fas fa-file-export"></i>
                                <p>
                                    Enviados
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" onclick="event.preventDefault(); document.getElementById('logout_form').submit();">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    Salir
                                </p>
                            </a>
                            <form action="{{ url('/logout') }}" id="logout_form" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('body')
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 2.0
            </div>
            <strong>Copyright &copy; 2022 <a href="https://rmp.mx">Ramírez Medellín, S.C.</a>.</strong> All rights reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

	{!! Html::script('plugins/jquery/jquery.min.js') !!}
	{!! Html::script('plugins/bootstrap/js/bootstrap.bundle.min.js') !!}
    {!! Html::script('plugins/toastr/toastr.min.js') !!}
    {!! Html::script('plugins/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('plugins/datatables/js/jquery.dataTables.js') !!}
    {!! Html::script('plugins/datatables/fixedColumns/js/dataTables.fixedColumns.js') !!}
    {!! Html::script('plugins/dropzone/dropzone.js') !!}

	{!! Html::script('dist/js/adminlte.min.js') !!}

    @yield('scripts')

</body>
</html>