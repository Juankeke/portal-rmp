@extends('master.coordinador')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Hacer un envío</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <strong>Mis clientes</strong>
                    </h3>
                </div>
                <div class="card-body">
                    @include('app.coordinador.send.layouts.index_clientes')
                </div>
            </div>      
        </div>
    </section>

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        get_clients();
    }); 

    function get_clients(){
        $('#loading_clientes').show();
        $.ajax({
            url: "{!! route('coord.get_clients') !!}",
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_clientes').hide();
                $.each( r, function( id, item ){
                    $('#clientes').append(                                                            
                        '<tr>' +
                            "<td width='40%'>" + item.cliente.razon_social + "</td>" +
                            "<td width='10%'>" + item.cliente.rfc + "</td>" +
                            "<td width='25%'>"+item.cliente.email+"</td>" +
                            "<td width='25%'><button class='btn btn-info btn-rounded btn-sm' onclick='view_send("+ item.cliente.id +");'>Enviar información</button></td>" +
                        '</tr>' 
                    );
                });
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function view_send(id){
        url = "{!! url( 'coordinador/cliente_" + id + "/view_send') !!}";
        //return window.open(url); 
        location.href = url;
    }

  </script>

@stop