<div class="table-responsive">
	<table class="table table-hover thead-index-consults table-sm m-b-0">
		<thead>
			<tr>				
				<th>RAZÓN SOCIAL</th>
				<th>RFC</th>
				<th>EMAIL</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="clientes"></tbody>
	</table>
	<center>
        <div id="loading_clientes" style="display:none;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </center>	
</div> 