{!! Form::open([
    'url' => 'coordinador/cliente_'.$cliente->id.'/send', 
    'method' => 'post',
    'id' => 'uploadFile',
    'class' => 'dropzone',
    'files' => true
]) !!}

<div class="dz-message">
    
    <div class="drag-icon-cph">
        <i class="fa fa-upload fa-3x fa-fw"></i>
    </div>

    <br><br>
    
    <h5>Arrastra y suelta los archivos o haz clic para cargarlos</h5>

</div>

<div class="fallback">
    <input name="file" type="file" id="files_dropzone" multiple/>
</div>

{!! Form::close() !!}