<div class="table-responsive">
	<table id="files" class="table table-hover thead-index-consults table-sm m-b-0">
		<thead>
			<tr>		
				<th></th>		
				<th>Archivo</th>
				<th></th>
				<th>Tamaño</th>
				<th>Receptor</th>
				<th>Fecha de envío</th>
				<th>Descargado</th>
			</tr>
		</thead>
	</table>
	<center>
        <div id="loading_files" style="display:none;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </center>	
</div> 