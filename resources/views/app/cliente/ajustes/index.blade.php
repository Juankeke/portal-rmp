@extends('master.cliente')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ajustes</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-warning color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Cambiar contraseña
                    </h3>
                </div>
                <div class="card-body">
                    @include('app.cliente.ajustes.layouts.change_password')
                </div>
            </div>      
        </div>
    </section>

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }); 

    function update(){
        $.ajax({
            url: "{!! route('cliente.update_password') !!}",
            type: 'PUT',
            dataType: 'json',
            data: $('#form_update_password').serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 1:
                        swal(r.title, r.message, 'success');
                        $("#form_update_password")[0].reset();
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                        break;
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                        break;
                }
            },
            error:function(r){
                errors(r);
            }
        });
    }

    function errors(r){
        errors_clean();
        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = r.responseText;
        var jd = JSON.parse(response);

        if(jd.errors.old_password != null){                
            $('#error_old_password').append("<div class='input-error' id='error_first_old_password'>" + jd.errors.old_password + "</div>");
            $('#old_password').addClass('is-invalid');
        }

        if(jd.errors.password != null){
            $('#error_new_password').append("<font class='input-error' id='error_first_password_edit'>" + jd.errors.password + "</font>");
            $('#new_password').addClass('is-invalid');
        }
    }

    function errors_clean(){
        $('#error_first_old_password').remove();
        $('#old_password').removeClass('is-invalid');

        $('#error_first_password_edit').remove();
        $('#new_password').removeClass('is-invalid');
    }

  </script>

@stop