{!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_update_password' ] ) !!} 

    <div class="form-row">

        <div class="col-md-4">
            <span class="label-form">Contraseña anterior</span> 
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" id="old_password" name="old_password" class="form-control" maxlength="20">
            </div>
            <div id="error_old_password"></div>
        </div>

        <div class="col-md-4">
            <span class="label-form">Nueva contraseña</span> 
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" id="new_password" name="password" class="form-control">
            </div>
            <small class="form-text text-muted">Mínimo 8 caracteres</small>
        </div> 

        <div class="col-md-4">
            <span class="label-form">Confirmar nueva contraseña</span> 
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" id="new_password_confirmation" name="password_confirmation" class="form-control">
            </div>
            <div id="error_new_password"></div>
        </div> 

    </div>                               

{!! Form::close() !!} 

<center>
    <button class="btn btn-warning btn-md" onclick="update();">Actualizar</button>
</center>