@extends('master.cliente')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Hacer un envío</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <strong>Mis contactos</strong>
                    </h3>
                </div>
                <div class="card-body">
                    @include('app.cliente.send.layouts.index_coords')
                </div>
            </div>      
        </div>
    </section>

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        get_coords();
    }); 

    function get_coords(){
        $('#loading_coords').show();
        $.ajax({
            url: "{!! route('cliente.get_coords') !!}",
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_coords').hide();
                $.each( r, function( id, item ){
                    $('#coords').append(                                                            
                        '<tr>' +
                            "<td width='40%'>" + item.coordinador.full_name + "</td>" +
                            "<td width='25%'>"+item.coordinador.email+"</td>" +
                            "<td width='25%'><button class='btn btn-info btn-rounded btn-sm' onclick='view_send("+ item.coordinador.id +");'>Enviar información</button></td>" +
                        '</tr>' 
                    );
                });
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function view_send(id){
        url = "{!! url( 'cliente/coord_" + id + "/view_send') !!}";
        //return window.open(url); 
        location.href = url;
    }

  </script>

@stop