@extends('master.cliente')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Hacer un envío</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Envío para {!! $coordinador->getFullNameAttribute() !!}
                    </h3>
                </div>
                <div class="card-body">
                    <strong class="h5">Información</strong>
                    <ul>
                        <li>Solo archivos: .zip, .rar y .7z</li>
                        <li>Solo 20 archivos por carga (arrastre 20 en 20)</li>
                    </ul>
                    <br>
                    @include('app.cliente.send.layouts.send_form')
                </div>
            </div>      
        </div>
    </section>

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }); 

    Dropzone.options.uploadFile = {
        autoProcessQueue: true, //false
        parallelUploads: 1,
        maxFiles: 20,
        maxFilesize: 1024, // Peso en Megabytes
        addRemoveLinks: true,
        acceptedFiles: '.zip,.rar,.7z',
        init: function () {
        }
        

        // FUNCION PARA ENVIAR MEDIANTE UN BOTON
        /*init: function() {
                var submitBtn = document.querySelector("#uploadImages");
                dropzoneImg = this;
                
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    dropzoneImg.processQueue();
                });

                this.on("success", 
                    dropzoneImg.processQueue.bind(dropzoneImg)
                );
        }*/
    }

  </script>

@stop