@extends('master.cliente')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Elementos recibidos</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title text-primary">
                        Archivos recibidos
                    </h3>
                </div>
                <div class="card-body">
                    <div align="right" style="margin-bottom: 10px;">
                        <button class="btn btn-danger btn-md" id="delete_files">
                            Eliminar seleccionados
                        </button>
                        <button class="btn btn-success btn-md" id="download_files">
                            Descargar seleccionados
                        </button>
                        <button class="btn btn-dark btn-md" id="refresh_files">
                            Refrescar tabla
                        </button>
                    </div>
                    @include('app.cliente.recibidos.layouts.index')
                </div>
            </div>
        </div>
    </section>

    @include('master.layouts.modal_loading')

@stop

@section('scripts')

    <script>

        $(document).ready(function(){ 

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            get_files();
        }); 

        function get_files(){
            $('#loading_files').show();
            $.ajax({
                url: "{!! route('cliente.get_received_files') !!}",
                type: 'GET',
                dataType: 'json',
                success:function(r){
                    $('#loading_files').hide();
                    show_files(r);
                },
                error:function(r){
                    console.log(r)
                }
            });
        }

        function show_files(response){
            var table_files = $('#files').DataTable({
                "processing": true,
                "scroller": true,
                "pageLength": 200,
                "data": response,
                //"bDestroy": true,  
                "order": [[5, 'desc']],      
                "columns":[       
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<center>" +
                            "<input name='file' type='checkbox' value='"+ data.id +"'>" +
                        "</center>";
                        }
                    },              
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            if(data.readed < 1){
                                return "<strong>"+data.nameOrg+"</strong>";
                            } else{
                                return data.nameOrg;
                            }
                        }
                    },
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<a href='{!! url('cliente/download_file_"+data.id+"/received') !!}'>Descargar <i class='fa fa-download'></i></a>";
                        }
                    },
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return data.size + ' mb';
                        }
                    },
                    {data: 'coordinador.full_name'},
                    {data: 'created'}
                ]
            });

            $('#delete_files').unbind().on("click", function(){

                var files = [];
                table_files.rows().nodes().to$().find(":checkbox[name=file]").each(function(){
                //$(":checkbox[name=obligacion]").each(function() {
                    if(this.checked){
                        // agregas cada elemento.
                        files.push($(this).val());
                    }
                });

                if(files.length){
                    $('#modal_loading').modal('show');
                    $.ajax({
                        url: "{!! route('cliente.delete_received_files') !!}",
                        type: 'POST',
                        dataType: 'json',
                        data: { 'files': JSON.stringify( files ) },
                        success: function(r){
                            setTimeout( function(){$("#modal_loading").modal('hide')}, 300 );
                            swal(r.title, r.message, 'success');
                            $('#files').dataTable().fnDestroy();
                            $(':checkbox[name=file]').prop('checked',false);
                            get_files();                                                        
                        },
                        error: function(data){
                            $('#modal_loading').modal('hide');
                            console.log( 'No se pudieron eliminar los archivos seleccionados' );
                        }
                    });
                } else{
                    swal("Información", "No se ha seleccionado ningún archivo para eliminar", "info");
                }                
            });

            $('#download_files').unbind().on( 'click', function(){
                var files = [];
                table_files.rows().nodes().to$().find(":checkbox[name=file]").each(function(){
                //$(":checkbox[name=obligacion]").each(function() {
                    if(this.checked){
                        // agregas cada elemento.
                        files.push($(this).val());
                    }
                });

                if(files.length){
                    $('#modal_loading').modal('show');
                    $.ajax({
                        url: "{!! route('cliente.download_received_files') !!}",
                        type: 'POST',
                        //dataType: 'JSON',
                        data: { 'files': JSON.stringify( files ) },
                        xhrFields: {
                            responseType: 'blob'
                        },
                        success: function(r){
                            setTimeout( function(){$("#modal_loading").modal('hide')}, 300 );

                            var blob = new Blob([r]);
                            var link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = "Download_Portal.zip";
                            link.click();

                            $(':checkbox[name=file]').prop('checked',false);
                            $('#files').dataTable().fnDestroy();
                            get_files();
                            
                        },
                        error: function(data){
                            $('#loading').hide();
                            console.log( 'No se pudieron descargar los archivos seleccionados' );
                        }
                    });
                } else{
                    swal("Información", "No se ha seleccionado ningún archivo para descargar", "info");
                }                
            });
        }        

        $('#refresh_files').unbind().on( 'click', function(){
            $('#files').dataTable().fnDestroy();
            get_files();
        });

    </script>

@stop