@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Ajustes</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Ajustes del sistema
                    </h3>
                </div>
                <div class="card-body">
                    @include('app.admin.ajustes.layouts.index')
                </div>
            </div>     
        </div>
    </section>

    @include('master.layouts.modal_loading')

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    }); 

    function reset_download_folder(){
        $('#modal_loading').modal('show');
        $.ajax({
            url: "{!! route('admin.reset_folder_downloads') !!}",
            type: 'GET',
            dataType: 'json',
            success:function(r){
                setTimeout( function(){$("#modal_loading").modal('hide')}, 300 );
                switch(r.status_answer){
                    case 1:
                        swal(r.title, r.message, 'success');
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                        break; 
                    default:
                        swal( 'ERROR CASE', 'case default','error');
                }
            },
            error:function(r){
                setTimeout( function(){$("#modal_loading").modal('hide')}, 300 );
                console.log(r);
            }
        });
    }

  </script>

@stop