<div class="modal fade bs-example-modal-sm" id="modal_edit_password" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-orange" id="mySmallModalLabel">Modificar contraseña de usuario | 
                    <font color="#000">
                        <strong id="nombre_usuario_password"></strong>
                        <i id="loading_form_edit_password" class="fas fa-spinner fa-spin fa-1x" style="display: none;"></i>
                    </font>
                </h5>
            </div>
            <div class="modal-body">

                {!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_update_password' ] ) !!} 

                    <input type="hidden" name="id_coord_pass" id="id_coord_pass">

                    <div class="form-row">

                        <div class="col-md-4">

                            <span class="label-form">Nueva contraseña</span> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" id="new_password" name="password" class="form-control" maxlength="20">
                            </div>

                            <small class="form-text text-muted">Mínimo 8 caracteres</small>

                        </div> 

                        <div class="col-md-4">

                            <span class="label-form">Confirmar nueva contraseña</span> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" id="new_password_confirmation" name="password_confirmation" class="form-control" maxlength="20">
                            </div>

                            <div id="error_new_password"></div>

                        </div> 

                    </div>                               

                {!! Form::close() !!}  

            </div>

            <div class="modal-footer">
                <button onclick="update_password();" class="btn btn-outline-warning btn-sm">Actualizar</button>
            </div>
                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


