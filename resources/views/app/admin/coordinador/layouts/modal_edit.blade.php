<div class="modal fade bs-example-modal-sm" id="modal_edit" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-orange">Modificar coordinador | 
                    <font color="#000">
                        <strong id="coord"></strong>
                        <i class="fas fa-spinner fa-spin fa-1x" style="display: none;"></i>
                    </font>
                </h5>
            </div>
            <div class="modal-body">

                {!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_update' ] ) !!}

                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="role_id" id="role_id">

                    <p><span class="text-danger">*</span> Obligatorios</p>

                    <div class="form-row">
                        <div class="col-md-4">
                            <label>Paterno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'paterno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'paterno_edit'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_paterno_edit"></div>
                        </div>
                        <div class="col-md-4">
                            <label>Materno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'materno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'materno_edit'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_materno_edit"></div>
                        </div>
                        <div class="col-md-4">
                            <label>Nombre(s)<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'nombre', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'nombre_edit'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_nombre_edit"></div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Correo electrónico<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                {!! Form::text(
                                    'email', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'id' => 'email_edit'
                                    ]
                                ) !!}

                            </div>
                            <div id="error_email_edit"></div>                            
                        </div>
                    </div>

                    <fieldset class="fieldset-detail">
                        <legend>Credenciales de acceso</legend>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label>Nombre de usuario<span class="text-danger">*</span></label> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    {!! Form::text(
                                        'username', 
                                        null, 
                                        [
                                            'class' => 'form-control',
                                            'autocomplete'=>'off',
                                            'id' => 'username_edit'
                                        ]
                                    ) !!}

                                </div>
                                <div id="error_username_edit"></div>                            
                            </div>
                            <div class="col-md-4">
                                <label>Status<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="activo" name="status" value="1">
                                            <label class="custom-control-label" for="activo">Activo</label>
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="inactivo" name="status" value="0">
                                            <label class="custom-control-label" for="inactivo">Inactivo</label>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </fieldset>

                {!! Form::close() !!}  

            </div>

            <div class="modal-footer">
                <button class="btn btn-warning btn-md" onclick="update();">Actualizar</button>
            </div>
                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


