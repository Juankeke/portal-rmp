<div class="modal fade bs-example-modal-sm" id="modal_store" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-info">Registrar coordinador</h5>
            </div>
            <div class="modal-body">

                {!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_store' ] ) !!} 

                    <p><span class="text-danger">*</span> Obligatorios</p>

                    <div class="form-row">
                        <div class="col-md-4">
                            <label>Paterno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'paterno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'paterno'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_paterno"></div>
                        </div>
                        <div class="col-md-4">
                            <label>Materno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'materno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'materno'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_materno"></div>
                        </div>
                        <div class="col-md-4">
                            <label>Nombre(s)<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'nombre', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'nombre'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_nombre"></div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Correo electrónico<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                {!! Form::text(
                                    'email', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'id' => 'email'
                                    ]
                                ) !!}

                            </div>
                            <div id="error_email"></div>                            
                        </div>
                    </div>

                    <fieldset class="fieldset-detail">
                        <legend>Credenciales de acceso</legend>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label>Nombre de usuario<span class="text-danger">*</span></label> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    {!! Form::text(
                                        'username', 
                                        null, 
                                        [
                                            'class' => 'form-control',
                                            'autocomplete'=>'off',
                                            'id' => 'username'
                                        ]
                                    ) !!}

                                </div>
                                <div id="error_username"></div>                            
                            </div>
                            <div class="col-md-4">
                                <label>Contraseña<span class="text-danger">*</span></label> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" id="password" name="password" class="form-control" maxlength="20">
                                </div>
                                <small class="form-text text-muted">Mínimo 8 caracteres</small>
                            </div>
                            <div class="col-md-4">
                                <label>Confirmar Contraseña<span class="text-danger">*</span></label> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" maxlength="20">
                                </div>
                                <div id="error_password"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            
                        </div>
                    </fieldset>

                {!! Form::close() !!}  

            </div>

            <div class="modal-footer">
                <button class="btn btn-info btn-md" onclick="store();">Guardar</button>
            </div>
                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


