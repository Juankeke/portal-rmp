@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Coordinadores</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Lista
                    </h3>
                </div>
                <div class="card-body">
                    <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#modal_store">
                        Nuevo
                    </button>

                    <br><br>

                    @include('app.admin.coordinador.layouts.index')        
                </div>
            </div>                
        </div>
    </section>

    @include('app.admin.coordinador.layouts.modal_store')
    @include('app.admin.coordinador.layouts.modal_edit')
    @include('app.admin.coordinador.layouts.modal_edit_password')

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        get_coords();
    }); 

    function get_coords(){
        $('#loading_data').show();
        $.ajax({
            url: "{!! route('admin.coordinador.get') !!}",
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_data').hide();
                show_coords(r);                  
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function show_coords(response){
        var table_coords = $('#coords').DataTable({
            "processing": true,
            "scroller": true,
            "pageLength": 200,
            "data": response,
            //"bDestroy": true,  
            "order": [[0, 'desc']],      
            "columns":[                     
                {data: 'full_name'},
                {data: 'email'},
                {data: 'user.username'},
                {data: 'user.status_user'},
                { 
                    data: null,
                    orderable: false,
                    render:function(data, type, row)
                    {
                        return "<button class='btn btn-warning btn-rounded btn-sm' onclick='edit("+ data.id +");' data-toggle='modal' data-target='#modal_edit'>Modificar</button> " + 
                        "<button class='btn btn-dark btn-rounded btn-sm' onclick='edit_password("+ data.id +");' data-toggle='modal' data-target='#modal_edit_password'>Modificar contraseña</button> " + 
                        "<button class='btn btn-primary btn-rounded btn-sm' onclick='relaciones("+ data.id +");'>Relaciones</button> ";
                    }
                }
            ]
        });
    }

    function store(){
        $.ajax({
            url: "{!!route('admin.coordinador.store')!!}",
            type: 'POST',
            dataType: 'json',
            data: $('#form_store').serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 1:
                        swal(r.title, r.message, 'success');
                        $('#modal_store').modal('toggle');
                        $('#coords').dataTable().fnDestroy();
                        get_coords();
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                        break;
                    case 3:
                        swal(r.title, r.message, 'info'); 
                        break;   
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                        break;
                }
            },
            error:function(r){
                errors_create(r);
            }
        });
    }

    function edit(id){
        $.ajax({
            url: 'coordinador/' + id + '/edit',
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#coord').html(r.full_name);
                $('#paterno_edit').val(r.paterno);
                $('#materno_edit').val(r.materno);
                $('#nombre_edit').val(r.nombre);
                $('#email_edit').val(r.email);
                $('#username_edit').val(r.user.username);
                $('#role_id').val(r.user.role_id);

                if(r.user.status == 1){
                    $('#activo').prop('checked',true);
                } else{
                    $('#inactivo').prop('checked',true);
                }

                $('#id').val(r.id);
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function update(){
        var id = $('#id').val();
        $.ajax({
            url: 'coordinador/' + id + '/update',
            type: 'put',
            dataType: 'json',
            data: $('#form_update').serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 0:
                        swal(r.title, r.message, 'info');
                        break;
                    case 1:
                        swal(r.title, r.message, 'success');
                        $('#modal_edit').modal('toggle');
                        $('#coords').dataTable().fnDestroy();
                        get_coords();
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                        break;   
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                        break;
                }
            },
            error:function(r){
                errors_update(r);
            }
        });
    }

    function edit_password(id){
        $('#loading_form_edit_password').show();
        $.ajax({
            url: 'coordinador/' + id + '/edit',
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_form_edit_password').hide();
                $('#nombre_usuario_password').html(r.full_name);
                $("#id_coord_pass").val(r.id);
            },
            error:function( data )
            {
                console.log( 'No se pudo obtener el usuario a editar la contraseña' );
                $( '#loading_form_edit_user' ).hide();
            }
        });    
    }

    function update_password(){
        var id = $("#id_coord_pass").val();

        $.ajax({
            url: 'coordinador/' + id + '/update_pass',
            type: 'PUT',
            dataType: 'json',
            data: $( '#form_update_password' ).serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 1:
                        swal( r.title, r.message, 'success' );
                        $('#modal_edit_password').modal( 'toggle' );
                        $('#coords').dataTable().fnDestroy();
                        get_coords();
                        break;
                    case 2:
                        swal( data.title, data.message, 'error' );
                        break;
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                }
            },
            error:function(data){
                errors_update_password(data);
            }
        });
    }

    function errors_create(r){
        errors_clean();
        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = r.responseText;
        var jd = JSON.parse(response);

        if(jd.errors.paterno != null){                
            $('#error_paterno').append("<div class='input-error' id='error_first_paterno'>" + jd.errors.paterno + "</div>");
            $('#paterno').addClass('is-invalid');
        }

        if(jd.errors.materno != null){                
            $('#error_materno').append("<div class='input-error' id='error_first_materno'>" + jd.errors.materno + "</div>");
            $('#materno').addClass('is-invalid');
        }

        if(jd.errors.nombre != null){                
            $('#error_nombre').append("<div class='input-error' id='error_first_nombre'>" + jd.errors.nombre + "</div>");
            $('#nombre').addClass('is-invalid');
        }

        if(jd.errors.email != null){                
            $('#error_email').append("<div class='input-error' id='error_first_email'>" + jd.errors.email + "</div>");
            $('#email').addClass('is-invalid');
        }

        if(jd.errors.username != null){                
            $('#error_username').append("<div class='input-error' id='error_first_username'>" + jd.errors.username + "</div>");
            $('#username').addClass('is-invalid');
        }

        if(jd.errors.password != null){                
            $('#error_password').append("<font class='input-error' id='error_first_password'>" + jd.errors.password + "</font>");
            $('#password').addClass('is-invalid');
        }
    }

    function errors_update(r){
        errors_clean();
        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = r.responseText;
        var jd = JSON.parse(response);

        if(jd.errors.paterno != null){                
            $('#error_paterno_edit').append("<div class='input-error' id='error_first_paterno_edit'>" + jd.errors.paterno + "</div>");
            $('#paterno_edit').addClass('is-invalid');
        }

        if(jd.errors.materno != null){                
            $('#error_materno_edit').append("<div class='input-error' id='error_first_materno_edit'>" + jd.errors.materno + "</div>");
            $('#materno_edit').addClass('is-invalid');
        }

        if(jd.errors.nombre != null){                
            $('#error_nombre_edit').append("<div class='input-error' id='error_first_nombre_edit'>" + jd.errors.nombre + "</div>");
            $('#nombre_edit').addClass('is-invalid');
        }

        if(jd.errors.email != null){                
            $('#error_email_edit').append("<div class='input-error' id='error_first_email_edit'>" + jd.errors.email + "</div>");
            $('#email_edit').addClass('is-invalid');
        }

        if(jd.errors.username != null){                
            $('#error_username_edit').append("<div class='input-error' id='error_first_username_edit'>" + jd.errors.username + "</div>");
            $('#username_edit').addClass('is-invalid');
        }
    }

    function errors_update_password(data){  
        errors_clean();

        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = data.responseText;
        var jsonData = JSON.parse(response);

        if(jsonData.errors.password != null){
            $('#error_new_password').append("<font class='input-error' id='error_first_password_edit'>" + jsonData.errors.password + "</font>");
            $('#new_password').addClass('is-invalid');
        }
    }

    function errors_clean(){
        $('#error_first_paterno').remove();
        $('#paterno').removeClass('is-invalid');
        $('#error_first_materno').remove();
        $('#materno').removeClass('is-invalid');
        $('#error_first_nombre').remove();
        $('#nombre').removeClass('is-invalid');
        $('#error_first_email').remove();
        $('#email').removeClass('is-invalid');
        $('#error_first_username').remove();
        $('#username').removeClass('is-invalid');
        $('#error_first_password').remove();
        $('#password').removeClass('is-invalid');

        $('#error_first_paterno_edit').remove();
        $('#paterno_edit').removeClass('is-invalid');
        $('#error_first_materno_edit').remove();
        $('#materno_edit').removeClass('is-invalid');
        $('#error_first_nombre_edit').remove();
        $('#nombre_edit').removeClass('is-invalid');
        $('#error_first_email_edit').remove();
        $('#email_edit').removeClass('is-invalid');
        $('#error_first_username_edit').remove();
        $('#username_edit').removeClass('is-invalid');

        $('#error_first_password_edit').remove();
        $('#new_password').removeClass('is-invalid');
    }

    $("#modal_store").on("hidden.bs.modal", function(){
        $("#form_store")[0].reset();   
        errors_clean();
    });

    $("#modal_edit").on("hidden.bs.modal", function(){
        errors_clean();
    });

    $("#modal_edit_password").on("hidden.bs.modal", function(){         
        $("#form_update_password")[0].reset(); 
        errors_clean();
    });

    function relaciones(id){
        url = "{!! url( 'admin/coordinador/" + id + "/relaciones') !!}";
        //return window.open(url); 
        location.href = url;
    }

  </script>

@stop