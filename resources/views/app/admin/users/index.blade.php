@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usuario</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title text-primary">
                        <strong>Cuentas de usuario</strong>
                    </h3>
                </div>
                <div class="card-body"> 
                    <div align="left">
                        <button class="btn btn-info" data-toggle="modal" data-target="#modal_store">
                            <i class="fa fa-plus"></i> Nuevo usuario
                        </button>
                    </div>

                    <br>

                    @include('app.admin.users.layouts.index')
                </div>
            </div>      
        </div>
    </section>

    @include('app.admin.users.layouts.modal_store')
    @include('app.admin.users.layouts.modal_edit')
    @include('app.admin.users.layouts.modal_edit_password')

@stop

@section('scripts')

    <script>

        $(document).ready(function(){ 

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            get_users();
        }); 

        function get_users(){
            $('#loading_users').show();
            $.ajax({
                url: "{!! route('admin.users.get') !!}",
                type: 'GET',
                dataType: 'json',
                success:function(data){
                    $('#loading_users').hide();
                    show_users(data.data);                  
                },
                error:function(data){
                    console.log(data);
                }
            });
        }

        function show_users(response){

            var table_users = $('#users').DataTable({
                "processing": true,
                "scroller": true,
                "data": response,
                //"bDestroy": true,  
                "order": [[0, 'desc']],      
                "columns":[ 
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<span class='text-info'>" + data.full_name + "</span>";
                        }
                    },
                    {data: 'username'},
                    {data: 'role.name'},
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<button class='btn btn-outline-warning btn-rounded btn-sm' onclick='edit("+ data.id +");' data-toggle='modal' data-target='#modal_edit'>Modificar</button> " +

                            "<button class='btn btn-outline-primary btn-rounded btn-sm' onclick='edit_password("+ data.id +",\""+data.full_name+"\");' data-toggle='modal' data-target='#modal_edit_password'>Cambiar contraseña</button>";
                        }
                    }
                ]
            });
        }

        function store(){
            $.ajax({
                //url: 'user_s/store',
                url: "{{ route('admin.users.store') }}",
                type: 'POST',
                dataType: 'json',
                data: $('#form_store').serialize(),
                success:function(data){
                    switch(data.status_answer){
                        case 1:
                            swal(data.title, data.message, 'success');
                            $('#modal_store').modal('toggle');
                            //$('#users').dataTable().fnDestroy();
                            //get_users();
                            break; 
                        default:
                            swal( 'ERROR CASE', 'case default', 'error' );
                    }
                },
                error:function(data)
                {
                    errors_create(data); 
                }
            });
        }

        function edit(id){
            $('#loading_form_edit_user').show();
            $.ajax({
                url: 'users/' + id + '/edit',
                type: 'GET',
                dataType: 'json',
                success:function( data ){
                    $('#loading_form_edit_user').hide();
                    $('#nombre_usuario').html(data.full_name);
                    $('#paterno_edit').val(data.paterno);
                    $('#materno_edit').val(data.materno);
                    $('#nombre_edit').val(data.nombre);
                    $('#username_edit').val(data.username);

                    if(data.status == 1){
                        $('#activo').prop('checked',true);
                    } else{
                        $('#inactivo').prop('checked',true);
                    }

                    $("#id").val(data.id);
                },
                error:function( data )
                {
                    $('#loading_form_edit_user').hide();
                }
            });
        }

        function update(){
            var id = $("#id").val();

            $.ajax({
                url: 'users/' + id + '/update',
                type: 'PUT',
                dataType: 'json',
                data: $('#form_update').serialize(),
                success:function(data){
                    switch(data.status_answer){
                        case 0:
                            swal( data.title, data.message, 'info');
                            break;
                        case 1:
                            swal( data.title, data.message, 'success');
                            $('#modal_edit').modal('toggle');
                            $("#users").dataTable().fnDestroy();
                            get_users();
                            break;                      
                        default:
                            swal('ERROR CASE', 'case default', 'error');
                    }
                },
                error:function(data){
                    errors_update(data);
                }
            });
        }

        function edit_password(id){
            $('#loading_form_edit_password').show();
            $.ajax({
                url: 'users/' + id + '/edit',
                type: 'GET',
                dataType: 'json',
                success:function(data){
                    $('#loading_form_edit_password').hide();
                    $('#nombre_usuario_password').html(data.full_name);
                    $("#id_user_password").val(data.id);
                },
                error:function( data )
                {
                    console.log( 'No se pudo obtener el usuario a editar la contraseña' );
                    $( '#loading_form_edit_user' ).hide();
                }
            });    
        }

        function update_password(){
            var id = $("#id_user_password").val();

            $.ajax({
                url: 'users/' + id + '/update_password',
                type: 'PUT',
                dataType: 'json',
                data: $( '#form_update_password' ).serialize(),
                success:function(data){
                    switch(data.status_answer){
                        case 1:
                            swal( data.title, data.message, 'success' );
                            $('#modal_edit_password').modal( 'toggle' );
                            $("#users").dataTable().fnDestroy();
                            get_users();
                            break;
                        case 2:
                            swal( data.title, data.message, 'error' );
                            break;
                        default:
                            swal( 'ERROR CASE', 'case default', 'error' );
                    }
                },
                error:function(data){
                    errors_update_password(data);
                }
            });
        }

        function errors_create(data){   
            errors_clean();

            toastr.error(
                'Hay algunos errores de validación, corrígelos.', 
                'Error de validación', 
                {
                    timeOut: 3000,
                    positionClass: "toast-top-full-width"
                }
            );

            var response = data.responseText;
            var jsonData = JSON.parse(response);

            if(jsonData.errors.paterno != null){                
                $('#error_paterno').append("<font class='input-error' id='error_first_paterno'>" + jsonData.errors.paterno + "</font>");
                $('#paterno').addClass('is-invalid');
            }

            if(jsonData.errors.materno != null){                
                $('#error_materno').append("<font class='input-error' id='error_first_materno'>" + jsonData.errors.materno + "</font>");
                $('#materno').addClass('is-invalid');
            }

            if(jsonData.errors.nombre != null){                
                $('#error_nombre').append("<font class='input-error' id='error_first_nombre'>" + jsonData.errors.nombre + "</font>");
                $('#nombre').addClass('is-invalid');
            }

            if(jsonData.errors.username != null){                
                $('#error_username').append("<font class='input-error' id='error_first_username'>" + jsonData.errors.username + "</font>");
                $('#username').addClass('is-invalid');
            }

            if(jsonData.errors.password != null){                
                $('#error_password').append("<font class='input-error' id='error_first_password'>" + jsonData.errors.password + "</font>");
                $('#password').addClass('is-invalid');
            }
        }

        function errors_update(data){   
            errors_clean();

            toastr.error(
                'Hay algunos errores de validación, corrígelos.', 
                'Error de validación', 
                {
                    timeOut: 3000,
                    positionClass: "toast-top-full-width"
                }
            );

            var response = data.responseText;
            var jsonData = JSON.parse(response);

            if(jsonData.errors.paterno != null){
                $('#error_paterno_edit').append("<font class='input-error' id='error_first_paterno_edit'>" + jsonData.errors.paterno + "</font>");
                $('#paterno_edit').addClass('is-invalid');
            }

            if(jsonData.errors.materno != null){
                $('#error_materno_edit').append("<font class='input-error' id='error_first_materno_edit'>" + jsonData.errors.materno + "</font>");
                $('#materno_edit').addClass('is-invalid');
            }

            if(jsonData.errors.nombre != null){
                $('#error_nombre_edit').append("<font class='input-error' id='error_first_nombre_edit'>" + jsonData.errors.nombre + "</font>");
                $('#nombre_edit').addClass('is-invalid');
            }

            if(jsonData.errors.username != null){                
                $('#error_username_edit').append("<font class='input-error' id='error_first_username_edit'>" + jsonData.errors.username + "</font>");
                $('#username_edit').addClass('is-invalid');
            }
        }

        function errors_update_password(data){  
            errors_clean();

            toastr.error(
                'Hay algunos errores de validación, corrígelos.', 
                'Error de validación', 
                {
                    timeOut: 3000,
                    positionClass: "toast-top-full-width"
                }
            );

            var response = data.responseText;
            var jsonData = JSON.parse(response);

            if(jsonData.errors.password != null){
                $('#error_new_password').append("<font class='input-error' id='error_first_password_edit'>" + jsonData.errors.password + "</font>");
                $('#new_password').addClass('is-invalid');
            }
        }

        function errors_clean(){
            $('#error_first_paterno').remove();
            $('#paterno').removeClass('is-invalid');

            $('#error_first_materno').remove();
            $('#materno').removeClass('is-invalid');

            $('#error_first_nombre').remove();
            $('#nombre').removeClass('is-invalid');

            $('#error_first_username').remove();
            $('#username').removeClass('is-invalid');

            $('#error_first_password').remove();
            $('#password').removeClass('is-invalid');


            $('#error_first_paterno_edit').remove();
            $('#paterno_edit').removeClass('is-invalid');

            $('#error_first_materno_edit').remove();
            $('#materno_edit').removeClass('is-invalid');

            $('#error_first_nombre_edit').remove();
            $('#nombre_edit').removeClass('is-invalid');

            $('#error_first_username_edit').remove();
            $('#username_edit').removeClass('is-invalid');


            $('#error_first_password_edit').remove();
            $('#new_password').removeClass('is-invalid');
        }

        //Al cerrar al modal ejecutarse...
        $("#modal_store").on("hidden.bs.modal", function(){
            $("#form_store")[0].reset();   
            errors_clean();
        });

        $("#modal_edit").on("hidden.bs.modal", function(){          
            errors_clean();
        });

        $("#modal_edit_password").on("hidden.bs.modal", function(){         
            $("#form_update_password")[0].reset(); 
            errors_clean();
        });

  </script>

@stop