<div class="modal fade bs-example-modal-sm" id="modal_edit" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-orange" id="mySmallModalLabel">Modificar usuario | 
                    <font color="#000">
                        <strong id="nombre_usuario"></strong>
                        <i id="loading_form_edit_user" class="fas fa-spinner fa-spin fa-1x" style="display: none;"></i>
                    </font>
                </h5>
            </div>
            <div class="modal-body">

                {!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_update' ] ) !!} 

                    <p><span class="text-danger">*</span> Obligatorios</p>

                    <input type="hidden" id="id">

                    <div class="form-row">

                        <div class="col-md-3">

                            <label>Apellido paterno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'paterno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'maxlength' => '18',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'paterno_edit'
                                    ]
                                ) !!}
                            </div>

                            <div id="error_paterno_edit"></div>
                            
                        </div> 

                        <div class="col-md-3">

                            <label>Apellido materno<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'materno', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'maxlength' => '18',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'materno_edit'
                                    ]
                                ) !!}
                            </div>

                            <div id="error_materno_edit"></div>
                            
                        </div> 

                        <div class="col-md-6">

                            <label>Nombre (s)<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'nombre', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'maxlength' => '18',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'nombre_edit'
                                    ]
                                ) !!}
                            </div>

                            <div id="error_nombre_edit"></div>
                            
                        </div> 

                    </div>

                    <br>

                    <div class="form-row">

                        <div class="col-md-4">

                            <label>Nombre de usuario<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                {!! Form::text(
                                    'username', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'maxlength' => '18',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'username_edit'
                                    ]
                                ) !!}

                            </div>

                            <small class="form-text text-muted">Mínimo 6 caracteres</small>

                            <div id="error_username_edit"></div>
                            
                        </div>  
                        
                    </div>

                {!! Form::close() !!}  

            </div>

            <div class="modal-footer">
                <button onclick="update();" class="btn btn-outline-info btn-sm">Guardar</button>
            </div>
                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


