<div class="table-responsive">
	<table id="files" class="table table-hover thead-index-consults table-sm m-b-0" style="font-size: 13px;">
		<thead>
			<tr>		
				<th></th>		
				<th>Archivo</th>
				<th>Tamaños</th>
				<th>Emisor</th>
				<th>Receptor</th>
				<th>Fecha de envío</th>
				<th><center>Descargado por <br>el cliente</center></th>
				<th><center>Eliminado por <br>coordinador</center></th>
				<th><center>Eliminado por <br>cliente</center></th>
			</tr>
		</thead>
	</table>
	<center>
        <div id="loading_files" style="display:none;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </center>	
</div> 