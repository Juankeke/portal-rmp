@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Archivos enviados por cliente</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Lista
                    </h3>
                </div>
                <div class="card-body">

                    <div align="right" style="margin-bottom: 10px;">
                        <button class="btn btn-danger btn-md" id="delete_files">
                            Eliminar archivos
                        </button>
                        <button class="btn btn-dark btn-md" id="refresh_files">
                            Refrescar tabla
                        </button>
                    </div>

                    @include('app.admin.cliente.enviados.layouts.index')        
                </div>
            </div>                
        </div>
    </section>
@stop

@section('scripts')

    <script>

        $(document).ready(function(){ 

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            get_files();
        });

        function get_files(){
            $('#loading_files').show();
            $.ajax({
                url: "{!! route('admin.cliente.get_enviados') !!}",
                type: 'GET',
                dataType: 'json',
                success:function(r){
                    $('#loading_files').hide();
                    show_files(r);
                },
                error:function(r){
                    console.log(r)
                }
            });
        }

        function show_files(response){
            var table_files = $('#files').DataTable({
                "processing": true,
                "scroller": true,
                "pageLength": 200,
                "data": response,
                //"bDestroy": true,  
                "order": [[5, 'desc']],      
                "columns":[       
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<center>" +
                            "<input name='file' type='checkbox' value='"+ data.id +"'>" +
                        "</center>";
                        }
                    },              
                    {data: 'nameOrg'},
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return data.size + ' mb';
                        }
                    },
                    {data: 'cliente.razon_social'},
                    {data: 'coordinador.full_name'},
                    {data: 'created'},
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            return "<center>"+data.status_readed+"</center>";
                        }
                    },
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            if(data.st_delete_coord < 1){
                                return "<center>No</center>";
                            } else{
                                return "<center><a href='#' onclick='rec_eliminado_coord("+ data.id +",\""+data.nameOrg+"\")'>Recuperar</a></center>";
                            }
                        }
                    },
                    { 
                        data: null,
                        orderable: false,
                        render:function(data, type, row)
                        {
                            if(data.st_delete_cliente < 1){
                                return "<center>No</center>";
                            } else{
                                return "<center><a href='#' onclick='rec_eliminado_cliente("+ data.id +",\""+data.nameOrg+"\")'>Recuperar</a></center>";
                            }
                        }
                    }
                ]
            });    

            $('#delete_files').unbind().on( 'click', function(){
                var files = [];
                table_files.rows().nodes().to$().find(":checkbox[name=file]").each(function(){
                //$(":checkbox[name=obligacion]").each(function() {
                    if(this.checked){
                        // agregas cada elemento.
                        files.push($(this).val());
                    }
                });

                if(files.length){
                    $('#modal_loading').modal('show');
                    $.ajax({
                        url: "{!! route('admin.cliente.eliminar_enviados') !!}",
                        type: 'POST',
                        dataType: 'JSON',
                        data: { 'files': JSON.stringify( files ) },
                        success: function(r){
                            setTimeout( function(){$("#modal_loading").modal('hide')}, 300 );
                            swal(r.title, r.message, 'success');
                            $('#files').dataTable().fnDestroy();
                            $(':checkbox[name=file]').prop('checked',false);
                            get_files();
                            
                        },
                        error: function(data){
                            $('#modal_loading').modal('hide');
                            console.log( 'No se pudieron eliminar los archivos seleccionados' );
                        }
                    });
                } else{
                    swal("Información", "No se ha seleccionado ningún archivo para eliminar", "info");
                }                
            });
        }

        function rec_eliminado_coord(id,item){
            swal(
                {
                    title: "<h4>Recuperar "+item+"</h4>",
                    text: "¿Seguro de recuperar el archivo para el coordinador?",
                    type: "warning",                        
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: "#DD6B55", 
                    confirmButtonText: "Recuperar",
                    confirmButtonColor: "#68E81F",
                    html:true,
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: "{!! url('admin/cliente/"+id+"/rec_eliminado_coord') !!}",
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                swal(data.title, data.message, 'success');
                                $('#files').dataTable().fnDestroy();                                
                                get_files();
                            },
                            error: function(data){
                                console.log(data);
                            }
                        });
                    }
                }
            );
            return false;
        }

        function rec_eliminado_cliente(id,item){
            swal(
                {
                    title: "<h4>Recuperar "+item+"</h4>",
                    text: "¿Seguro de recuperar el archivo para el cliente?",
                    type: "warning",                        
                    cancelButtonText: 'Cancelar',
                    cancelButtonColor: "#DD6B55", 
                    confirmButtonText: "Recuperar",
                    confirmButtonColor: "#68E81F",
                    html:true,
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function(isConfirm){
                    if (isConfirm){
                        $.ajax({
                            url: "{!! url('admin/cliente/"+id+"/rec_eliminado_cliente') !!}",
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                swal(data.title, data.message, 'success');
                                $('#files').dataTable().fnDestroy();                                
                                get_files();
                            },
                            error: function(data){
                                console.log(data);
                            }
                        });
                    }
                }
            );
            return false;
        }

        $('#refresh_files').unbind().on( 'click', function(){
            $('#files').dataTable().fnDestroy();
            get_files();
        });

    </script>
@stop