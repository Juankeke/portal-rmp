<div class="modal fade bs-example-modal-sm" id="modal_edit" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-orange">Modificar cliente | 
                    <font color="#000">
                        <strong id="cliente"></strong>
                        <i class="fas fa-spinner fa-spin fa-1x" style="display: none;"></i>
                    </font>
                </h5>
            </div>
            <div class="modal-body">

                {!! Form::open( [ '', 'class' => 'form-horizontal', 'id' => 'form_update' ] ) !!} 

                    <p><span class="text-danger">*</span> Obligatorios</p>

                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="role_id" id="role_id">

                    <div class="form-row">
                        <div class="col-md-12">
                            <label>Razón social<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'razon_social', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'id' => 'razon_social_edit'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_razon_social_edit"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <label>RFC<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                {!! Form::text(
                                    'rfc', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'onkeyup' => 'changeToUpperCase(this)',
                                        'maxlength' => '13',
                                        'id' => 'rfc_edit'
                                    ]
                                ) !!}
                            </div>
                            <div id="error_rfc_edit"></div>
                        </div>

                        <div class="col-md-6">
                            <label>Correo electrónico<span class="text-danger">*</span></label> 
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                {!! Form::text(
                                    'email', 
                                    null, 
                                    [
                                        'class' => 'form-control',
                                        'autocomplete'=>'off',
                                        'id' => 'email_edit'
                                    ]
                                ) !!}

                            </div>
                            <div id="error_email_edit"></div>                            
                        </div>
                    </div>

                    <fieldset class="fieldset-detail">
                        <legend>Credenciales de acceso</legend>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label>Nombre de usuario<span class="text-danger">*</span></label> 
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    {!! Form::text(
                                        'username', 
                                        null, 
                                        [
                                            'class' => 'form-control',
                                            'autocomplete'=>'off',
                                            'id' => 'username_edit'
                                        ]
                                    ) !!}

                                </div>
                                <div id="error_username_edit"></div>                            
                            </div>
                            <div class="col-md-4">
                                <label>Status<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="activo" name="status" value="1">
                                            <label class="custom-control-label" for="activo">Activo</label>
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="inactivo" name="status" value="0">
                                            <label class="custom-control-label" for="inactivo">Inactivo</label>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </fieldset>

                {!! Form::close() !!}  

            </div>

            <div class="modal-footer">
                <button class="btn btn-warning btn-md" onclick="update();">Actualizar</button>
            </div>
                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


