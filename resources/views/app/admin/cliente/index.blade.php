@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Clientes</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        Lista
                    </h3>
                </div>
                <div class="card-body">
                    <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#modal_store">
                        Nuevo
                    </button>

                    <br><br>

                    @include('app.admin.cliente.layouts.index')         
                </div>
            </div>     
        </div>
    </section>

    @include('app.admin.cliente.layouts.modal_store')
    @include('app.admin.cliente.layouts.modal_edit')
    @include('app.admin.cliente.layouts.modal_edit_password')

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        get_clients();
    }); 

    function get_clients(){
        $('#loading_data').show();
        $.ajax({
            url: "{!! route('admin.cliente.get') !!}",
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_data').hide();
                show_clients(r);                  
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function show_clients(response){
        var table_coords = $('#clients').DataTable({
            "processing": true,
            "scroller": true,
            "pageLength": 200,
            "data": response,
            //"bDestroy": true,  
            "order": [[0, 'desc']],      
            "columns":[                     
                {data: 'razon_social'},
                {data: 'rfc'},
                {data: 'email'},
                {data: 'user.username'},
                {data: 'user.status_user'},
                { 
                    data: null,
                    orderable: false,
                    render:function(data, type, row)
                    {
                        return "<button class='btn btn-warning btn-rounded btn-sm' onclick='edit("+ data.id +");' data-toggle='modal' data-target='#modal_edit'>Modificar</button> " + 
                        "<button class='btn btn-dark btn-rounded btn-sm' onclick='edit_password("+ data.id +");' data-toggle='modal' data-target='#modal_edit_password'>Modificar contraseña</button> ";
                    }
                }
            ]
        });
    }

    function store(){
        $.ajax({
            url: "{!!route('admin.cliente.store')!!}",
            type: 'POST',
            dataType: 'json',
            data: $('#form_store').serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 1:
                        swal(r.title, r.message, 'success');
                        $('#modal_store').modal('toggle');
                        $('#clients').dataTable().fnDestroy();
                        get_clients();
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                    case 3:
                        swal(r.title, r.message, 'info');    
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                }
            },
            error:function(r){
                errors_create(r);
            }
        });
    }

    function edit(id){
        $.ajax({
            url: 'cliente/' + id + '/edit',
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#cliente').html(r.razon_social);
                $('#razon_social_edit').val(r.razon_social);
                $('#rfc_edit').val(r.rfc);
                $('#email_edit').val(r.email);
                $('#username_edit').val(r.user.username);
                $('#role_id').val(r.user.role_id);

                if(r.user.status == 1){
                    $('#activo').prop('checked',true);
                } else{
                    $('#inactivo').prop('checked',true);
                }

                $('#id').val(r.id);
            },
            error:function(r){
                console.log(r);
            }
        });
    }

    function update(){
        var id = $('#id').val();
        $.ajax({
            url: 'cliente/' + id + '/update',
            type: 'put',
            dataType: 'json',
            data: $('#form_update').serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 0:
                        swal(r.title, r.message, 'info');
                        break;
                    case 1:
                        swal(r.title, r.message, 'success');
                        $('#modal_edit').modal('toggle');
                        $('#clients').dataTable().fnDestroy();
                        get_clients();
                        break; 
                    case 2:
                        swal(r.title, r.message, 'error');
                        break;   
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                        break;
                }
            },
            error:function(r){
                errors_update(r);
            }
        });
    }

    function edit_password(id){
        $('#loading_form_edit_password').show();
        $.ajax({
            url: 'cliente/' + id + '/edit',
            type: 'GET',
            dataType: 'json',
            success:function(r){
                $('#loading_form_edit_password').hide();
                $('#nombre_cliente_password').html(r.razon_social);
                $("#id_cliente").val(r.id);
            },
            error:function( data )
            {
                console.log( 'No se pudo obtener el usuario a editar la contraseña' );
                $( '#loading_form_edit_user' ).hide();
            }
        });    
    }

    function update_password(){
        var id = $("#id_cliente").val();

        $.ajax({
            url: 'cliente/' + id + '/update_pass',
            type: 'PUT',
            dataType: 'json',
            data: $( '#form_update_password' ).serialize(),
            success:function(r){
                switch(r.status_answer){
                    case 1:
                        swal( r.title, r.message, 'success' );
                        $('#modal_edit_password').modal( 'toggle' );
                        $('#clients').dataTable().fnDestroy();
                        get_clients();
                        break;
                    case 2:
                        swal( data.title, data.message, 'error' );
                        break;
                    default:
                        swal( 'ERROR CASE', 'case default', 'error' );
                }
            },
            error:function(data){
                errors_update_password(data);
            }
        });
    }

    function errors_create(r){
        errors_clean();
        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = r.responseText;
        var jd = JSON.parse(response);

        if(jd.errors.razon_social != null){                
            $('#error_razon_social').append("<div class='input-error' id='error_first_razon_social'>" + jd.errors.razon_social + "</div>");
            $('#razon_social').addClass('is-invalid');
        }

        if(jd.errors.rfc != null){                
            $('#error_rfc').append("<div class='input-error' id='error_first_rfc'>" + jd.errors.rfc + "</div>");
            $('#rfc').addClass('is-invalid');
        }

        if(jd.errors.email != null){                
            $('#error_email').append("<div class='input-error' id='error_first_email'>" + jd.errors.email + "</div>");
            $('#email').addClass('is-invalid');
        }

        if(jd.errors.username != null){                
            $('#error_username').append("<div class='input-error' id='error_first_username'>" + jd.errors.username + "</div>");
            $('#username').addClass('is-invalid');
        }

        if(jd.errors.password != null){                
            $('#error_password').append("<font class='input-error' id='error_first_password'>" + jd.errors.password + "</font>");
            $('#password').addClass('is-invalid');
        }
    }

    function errors_update(r){
        errors_clean();
        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = r.responseText;
        var jd = JSON.parse(response);

        if(jd.errors.razon_social != null){                
            $('#error_razon_social_edit').append("<div class='input-error' id='error_first_razon_social_edit'>" + jd.errors.razon_social + "</div>");
            $('#razon_social_edit').addClass('is-invalid');
        }

        if(jd.errors.rfc != null){                
            $('#error_rfc_edit').append("<div class='input-error' id='error_first_rfc_edit'>" + jd.errors.rfc + "</div>");
            $('#rfc_edit').addClass('is-invalid');
        }

        if(jd.errors.email != null){                
            $('#error_email_edit').append("<div class='input-error' id='error_first_email_edit'>" + jd.errors.email + "</div>");
            $('#email_edit').addClass('is-invalid');
        }

        if(jd.errors.username != null){                
            $('#error_username_edit').append("<div class='input-error' id='error_first_username_edit'>" + jd.errors.username + "</div>");
            $('#username_edit').addClass('is-invalid');
        }
    }

    function errors_update_password(data){  
        errors_clean();

        toastr.error(
            'Hay algunos errores de validación, corrígelos.', 
            'Error de validación', 
            {
                timeOut: 3000,
                positionClass: "toast-top-full-width"
            }
        );

        var response = data.responseText;
        var jsonData = JSON.parse(response);

        if(jsonData.errors.password != null){
            $('#error_new_password').append("<font class='input-error' id='error_first_password_edit'>" + jsonData.errors.password + "</font>");
            $('#new_password').addClass('is-invalid');
        }
    }

    function errors_clean(){
        $('#error_first_razon_social').remove();
        $('#razon_social').removeClass('is-invalid');
        $('#error_first_rfc').remove();
        $('#rfc').removeClass('is-invalid');
        $('#error_first_email').remove();
        $('#email').removeClass('is-invalid');
        $('#error_first_username').remove();
        $('#username').removeClass('is-invalid');
        $('#error_first_password').remove();
        $('#password').removeClass('is-invalid');

        $('#error_first_razon_social_edit').remove();
        $('#razon_social_edit').removeClass('is-invalid');
        $('#error_first_rfc_edit').remove();
        $('#rfc_edit').removeClass('is-invalid');
        $('#error_first_email_edit').remove();
        $('#email_edit').removeClass('is-invalid');
        $('#error_first_username_edit').remove();
        $('#username_edit').removeClass('is-invalid');

        $('#error_first_password_edit').remove();
        $('#new_password').removeClass('is-invalid');
    }

    $("#modal_store").on("hidden.bs.modal", function(){
        $("#form_store")[0].reset();   
        errors_clean();
    });

    $("#modal_edit").on("hidden.bs.modal", function(){
        errors_clean();
    });

    $("#modal_edit_password").on("hidden.bs.modal", function(){         
        $("#form_update_password")[0].reset(); 
        errors_clean();
    });


  </script>

@stop