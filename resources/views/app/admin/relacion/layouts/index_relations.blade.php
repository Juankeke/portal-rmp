<div class="table-responsive">
	<table class="table table-hover thead-index-consults table-sm m-b-0">
		<thead>
			<tr class="bg-success">
				<th>RAZÓN SOCIAL</th>
				<th>RFC</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="relations"></tbody>
	</table>
	<center>
        <div id="loading_relations" style="display: none;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </center>	
</div>
