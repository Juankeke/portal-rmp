<div class="table-responsive">
	<table class="table table-hover thead-index-consults table-sm m-b-0">
		<thead>
			<tr class="bg-primary">				
				<th><center>SELECCIONAR</center></th>
				<th>RAZÓN SOCIAL</th>
				<th>RFC</th>
			</tr>
		</thead>
		<tbody id="clients"></tbody>
	</table>
	<center>
        <div id="loading_data" style="display: none;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </center>	
</div>

<br>

<div align="center">
    <button onclick="store();" class="btn btn-success">Agregar selección</button>
</div>