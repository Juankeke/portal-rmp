@extends('master.admin')

@section('head')
@stop

@section('body')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Relaciones</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title text-primary">
                        <strong>{!! $coordinador->full_name !!}</strong>
                    </h3>
                </div>
                <div class="card-body"> 
                    <h5 class="text-info">Relacionados</h5>

                    @include('app.admin.relacion.layouts.index_relations')

                    <hr>

                    <h5 class="text-info">Clientes para relacionar</h5>
                    @include('app.admin.relacion.layouts.index_clients')       
                </div>
            </div>      
        </div>
    </section>

@stop

@section('scripts')

  <script>

    $(document).ready(function(){ 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        get_clients();
        get_relations();
    }); 

    function get_clients(){
        $('#clients').empty();
        $('#loading_data').show();
        $.ajax({
            url: 'relaciones/get_clients',
            type: 'GET',
            dataType: 'json',
            success:function(data){
                $('#loading_data').hide();

                $.each( data, function( id, item ){
                    $('#clients').append(                                                            
                        '<tr>' +
                            "<td width='10%'><center><input name='razon_social' type='checkbox' value='"+item.id+"'></center></td>" +
                            "<td width='40%'>" + item.razon_social + "</td>" +
                            "<td width='55%'>"+item.rfc+"</td>" +
                        '</tr>' 
                    );
                });
            },
            error:function(data){
                console.log(data);
            }
        });
    }

    function get_relations(){
        $('#relations').empty();
        $('#loading_relations').show();
        $.ajax({
            url:'relaciones/get',
            type: 'GET',
            dataType: 'json',
            success:function(data){
                $('#loading_relations').hide();

                $.each( data, function( id, item ){
                    $('#relations').append(                                                            
                        '<tr>' +
                            "<td width='40%'>" + item.cliente.razon_social + "</td>" +
                            "<td width='15%'>"+item.cliente.rfc+"</td>" +
                            "<td width='35%'><a href='#' onclick='destroy("+ item.id +",\""+item.cliente.razon_social+"\")' class='btn btn-sm btn-rounded btn-danger'>Eliminar</a></td>" +
                        '</tr>' 
                    );
                });
            },
            error:function(data){
                console.log(data);
            }
        });
    }

    function store(){
        var clientes = [];
        $(":checkbox[name=razon_social]").each(function() {
            if (this.checked) {
                clientes.push($(this).val());
            }
        });

        if(clientes.length) 
        {
            $.ajax({
                url: 'relaciones/store',
                type: 'POST',
                dataType: 'json',
                data: {'clientes': JSON.stringify(clientes)},
                success:function(data){
                    switch(data.status_answer){
                        case 1:                            
                            get_relations();
                            swal(data.title, data.message, 'success');
                            break;
                        default:
                            swal( 'ERROR CASE', 'case default', 'error');
                    }
                    $(':checkbox[name=razon_social]').prop('checked',false);
                },
                error:function(data){
                    console.log(data)
                }
            });
        } else{
            swal("Información", "Debes seleccionar algún cliente para relacionar con el coordinador", "info");
        }        
    }

    function destroy(id, item){
        swal(
            {
                title: "<h4>Eliminar "+item+"</h4>",
                text: "¿Seguro de eliminar la relación con este cliente?",
                type: "warning",                        
                cancelButtonText: 'Cancelar',
                cancelButtonColor: "#DD6B55", 
                confirmButtonText: "Eliminar",
                confirmButtonColor: "#68E81F",
                html:true,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                closeOnConfirm: false
            },
            function(isConfirm){
                if (isConfirm){
                    $.ajax({
                        url: "relaciones/" + id + "/destroy",
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            swal(data.title, data.message, 'success');                                
                            get_relations();
                        },
                        error: function(data){
                            console.log(data);
                        }
                    });
                }
            }
        );
        return false;
    }
    

  </script>

@stop