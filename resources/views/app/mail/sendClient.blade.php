<!DOCTYPE html>
<html>
<head>
    <title>Portal RMP</title>
    <meta charset="utf-8">

    <style type="text/css">
        .table-message{
            max-width: 700px; 
            padding: 10px; 
            margin:0 auto; 
            border-collapse: collapse; 
            border: 2px solid #F8F8F8; 
        }

        .bussiness{
            font-size: 20px;
            color: #34495E; 
            margin: 4% 10% 2%;
            text-align: justify;
        }

        .message{
            text-align:center; 
            margin: 20px;
        }
    </style>
</head>

<body>

<center>

    <table class="table-message">
        <tr>
            <td style="background-color: #F8F8F8;">
                <div class="bussiness">
                    <center>
                        <strong>
                            Portal Ramírez Medellín, S.C.
                        </strong>
                        <br>
                        Contadores públicos y abogados
                    </center>
                </div>
            </td>
        </tr>   
        <tr>
            <td style="background-color: #F8F8F8;">
                <div class="message">
                    <h3 style="color: #FDA113">Hola {!! $details['coordinador'] !!}</h3>
                    <p style=" margin: 2px; font-size: 17px;">
                        <strong>
                            {!! auth()->user()->cliente->razon_social !!}
                        </strong> 
                        te envió el archivo 
                        <strong>{!! $details['archivo'] !!}</strong>
                        el día {!! $details['fecha'] !!}
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color: #FCFCFC; border: 2px solid #ecf0f1">
                <div style="color: #34495E; margin: 4% 10% 2%; text-align: justify; font-family: 'Lato', sans-serif;">              
                    <p style="margin: 2px; font-size: 11px; color: #757575">
                        Este email se ha generado automáticamente. Por favor, no conteste a este email. 
                        Si tiene alguna pregunta o necesita ayuda, por favor, envíe un correo a la siguiente dirección: 
                        <font color="#4584D7">sistemas@rmp.mx</font>
                    </p>                    
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color: #FCFCFC; border: 2px solid #ecf0f1">
                <div style="color: #34495E; margin: 4% 10% 2%; text-align: justify; font-family: 'Lato', sans-serif;">              
                    <p style="margin: 2px; font-size: 11px; color: #0F2D3C">
                        <strong>AVISO LEGAL EN CORREOS ELECTRÓNICOS</strong>
                    </p>
                    <p style="margin: 2px; font-size: 11px; color: #3C6377">
                        Este correo electrónico y, en su caso, cualquier fichero anexo al mismo, 
                        contiene información de carácter confidencial exclusivamente dirigida a su destinatario o destinatarios. 
                        Queda prohibida su divulgación, copia o distribución a terceros sin la previa autorización 
                        escrita de Ramírez Medellín, S.C. En caso de no ser usted la persona a la que fuera dirigido este 
                        mensaje y a pesar de ello está continúa leyéndolo, ponemos en su conocimiento que está cometiendo 
                        un acto ilícito en virtud de la legislación vigente en la actualidad, por lo que deberá dejarlo de leer automáticamente.
                    </p>
                    <p style="margin: 2px; font-size: 11px; color: #3C6377">
                        Ramírez Medellín, S.C. no es responsable de su integridad, exactitud, 
                        o de lo que acontezca cuando el correo electrónico circula por las infraestructuras de 
                        comunicaciones electrónicas públicas. En el caso de haber recibido este correo electrónico por error, 
                        se ruega notificar inmediatamente esta circunstancia mediante reenvío a la dirección electrónica del remitente.
                    </p>
                    <p style="margin: 2px; font-size: 11px; color: #3C6377">
                        El correo electrónico vía Internet no permite asegurar la confidencialidad de los mensajes que se transmiten 
                        ni su integridad o correcta recepción, por lo que Ramírez Medellín, S.C. no asume ninguna responsabilidad que pueda 
                        derivarse de este hecho.
                    </p>
                    <p style="margin: 2px; font-size: 11px; color: #3C6377">
                        No imprima este correo si no es necesario. Ahorrar papel protege el medio ambiente.
                    </p>                    
                </div>
            </td>
        </tr>       
    </table>
</center>

</body>
</html>

